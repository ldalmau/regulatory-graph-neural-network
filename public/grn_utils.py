import os
import pandas as pd
from pathlib import Path


# Utility functions :
def create_dir(path, name, network, dataset) :
	"""
	utility function that make a dir if it don't exist or has been supress at the start of the script.
	"""
	algo_path = os.path.join(path, name, network, dataset)
	if not Path(algo_path).exists() :
		os.makedirs(algo_path)
	return os.path.join(path, name, network, dataset, "rankedEdges.csv")

#Def of functions :
# 	Import datasets
def find__iter(path) :
	"""
	Functions to find N_iter_regressors. If > 50, then set it to 50.
	"""
	data = pd.read_csv(path, sep=",", index_col=0)
	n_iter = data.shape[0]
	if n_iter > 50 :
		return 50
	else :
		return n_iter