# Import
# 	Run
import random
import copy
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor, ExtraTreesRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score, mean_absolute_error, r2_score, mean_absolute_percentage_error
from sklearn.inspection import permutation_importance
import tools_lisa 
from tools_lisa import prepare_data, prepare_for_regressor, parallel_plot
from grn_utils import create_dir
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from eli5.permutation_importance import get_score_importances
import numpy as np
import shap
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg') # Disable display, necessary due to WSL2 (Windows Subsystem for Linux) that can't open Linux windows

tf.get_logger().setLevel('ERROR') # Suppress a warning that is often display but not relevan

import os
import shutil
import time
from tqdm import tqdm
import itertools

# 	tensorflow neural network architecture
def build_and_compile_nn_simple_model(RAND_SEED):
	"""
	Model architecture from : DeepImpute: an accurate, fast, and scalable deep neural network method to impute single-cell RNA-seq data
	Cédric Arisdakessian, Olivier Poirion, Breck Yunits, Xun Zhu and Lana X. Garmire 
	"""
	model = keras.Sequential([
		layers.Dense(250, activation='relu'),
		layers.Dense(250, activation='relu'),
		layers.Dense(1)
	])
	
	tf.keras.utils.set_random_seed(RAND_SEED)

	model.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001))
	return model

def build_and_compile_nn_complex_model(RAND_SEED):
	"""
	Model architecture from : DeepImpute: an accurate, fast, and scalable deep neural network method to impute single-cell RNA-seq data
	Cédric Arisdakessian, Olivier Poirion, Breck Yunits, Xun Zhu and Lana X. Garmire 
	"""
	model = keras.Sequential([
		layers.Dense(256, activation='relu'),
		layers.Dropout(0.2),
		layers.Dense(1)
	])
	
	tf.keras.utils.set_random_seed(RAND_SEED)

	model.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001))
	return model

def build_and_compile_nn_functional_model(number, x_train, RAND_SEED):
	"""
	Model architecture from : DeepImpute: an accurate, fast, and scalable deep neural network method to impute single-cell RNA-seq data
	Cédric Arisdakessian, Olivier Poirion, Breck Yunits, Xun Zhu and Lana X. Garmire 
	"""
	model_input = []
	model_pred = []
	model_drop = []
	for i in range(number) :
		model_input.append(keras.Input(shape=(x_train.shape[1],)))
		model_pred.append(layers.Dense(256, activation='relu')(model_input[i]))
		model_drop.append(layers.Dropout(0.2)(model_pred[i]))

	x = layers.concatenate(model_drop)
	model_out = layers.Dense(number)(x)

	model = keras.Model(
		inputs= model_input,
		outputs=[model_out],
		)
	
	model.compile(
		optimizer= tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001),
		loss='mean_squared_error',
		)
	
	tf.keras.utils.set_random_seed(RAND_SEED)
	return model

def compute_RF(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, RF_KWARGS, Comp_FI = True, Comp_PI = True):
	"""
	Function by Lisa Chabrier to compute prediction for regressor Tree, adaptated to get Feature selection from 3 different ways
	Base : Compute Shapley value, if Comp_FI = True, compute feature importance tree by scikit-learn like in Arboreto, if Comp_PI = True, compute permutation importance
	"""
	start_RF = time.time()

	# Initialize R2 scores
	r2_list = []

	# SHAP initializer
	RF_path = create_dir(path, "SH_RF", network, dataset)
	outFile = open(RF_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Feature importance initializer
	if Comp_FI == True :
		RF_FI_path = create_dir(path, "FI_RF", network, dataset)
		outFile2 = open(RF_FI_path,'w')
		outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Permutation importance initializer
	if Comp_PI == True :
		RF_PI_path = create_dir(path, "PI_RF", network, dataset)
		outFile3 = open(RF_PI_path,'w')
		outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')


	training_time_list = []
	feature_time_list = []
	permutation_time_list = []
	shap_time_list = []
	# Learning model for each Target Gene
	for target_gene_index in tqdm(rand_index):
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Normalisation of the Target Gene
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		temp_RF = time.time()
		############### N_jobs can be set to a convenient value (-1 is suppose to automatically select the right number of cores to use)
		RF = RandomForestRegressor(random_state=RAND_SEED, n_jobs = RF_KWARGS["n_jobs"] , n_estimators = RF_KWARGS["n_estimators"],
                                   max_features = RF_KWARGS["max_features"]) # args are from arboreto/core.py
		RF.fit(x_train, y_train)
		training_time_list.append(time.time() - temp_RF)

		predictions = RF.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))

		# Create labels for indexing evaluation metrics
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		if Comp_FI == True :
			temp_RF = time.time()
			importances = RF.feature_importances_
			FI_importances = pd.Series(importances, index=feature_names).sort_values(0, ascending = False)
			feature_time_list.append(time.time() - temp_RF)

		if Comp_PI == True :
			temp_RF = time.time()
			importances = permutation_importance(RF, x_test, y_test, random_state = RAND_SEED, scoring = "r2")
			PI_importances = pd.Series(importances.importances_mean, index=feature_names).sort_values(0, ascending = False)
			permutation_time_list.append(time.time() - temp_RF)

		temp_RF = time.time()
		# Get Shapley values
		explainer = shap.Explainer(RF)
		shap_val = np.array(explainer.shap_values(x_test))
		shap_vector = np.sum(abs(shap_val), axis = 0).T
		shap_time_list.append(time.time() - temp_RF)
		
		# Writing result to file for evaluation
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				if Comp_FI == True :
					outFile2.write('\t'.join([gene_names[i],target_gene_name,str(FI_importances[j])])+'\n')
				if Comp_PI == True :
					outFile3.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances[j])])+'\n')
				j += 1

	total_time = time.time() - start_RF
	training_time = sum(training_time_list)/len(training_time_list)
	feature_time = sum(feature_time_list)/len(feature_time_list)
	permutation_time = sum(permutation_time_list)/len(permutation_time_list)
	shap_time = sum(shap_time_list)/len(shap_time_list)

	with open("Time_log.txt", "a") as f :
		f.write("\n")
		f.write("Temps d'execution de RF : %i \n" %(total_time))
		f.write("Temps d'apprentissage moyen pour RF pour 1 gène : %i \n" %(training_time))
		f.write("Temps de calcul moyen du FI pour RF pour 1 gène : %i \n" %(feature_time))
		f.write("Temps de calcul moyen du PI pour RF pour 1 gène : %i \n" %(permutation_time))
		f.write("Temps de calcul moyen de SHAP pour RF pour 1 gène : %i \n" %(shap_time))

	outFile.close()
	if Comp_FI == True :
		outFile2.close()
	if Comp_PI == True :
		outFile3.close()
	return r2_list

def compute_GBM(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, GBM_KWARGS, Comp_FI = True, Comp_PI = True):
	"""
	Function by Lisa Chabrier to compute prediction for regressor Tree, adaptated to get Feature selection from 3 different ways
	Base : Compute Shapley value, if Comp_FI = True, compute feature importance tree by scikit-learn like in Arboreto, if Comp_PI = True, compute permutation importance
	"""
	start_GBM = time.time()

	# Initialize R2 scores
	r2_list = []

	# SHAP initializer
	GBM_path = create_dir(path, "SH_GBM", network, dataset)
	outFile = open(GBM_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Feature importance initializer
	if Comp_FI == True :
		GBM_FI_path = create_dir(path, "FI_GBM", network, dataset)
		outFile2 = open(GBM_FI_path,'w')
		outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Permutation importance initializer
	if Comp_PI == True :
		GBM_PI_path = create_dir(path, "PI_GBM", network, dataset)
		outFile3 = open(GBM_PI_path,'w')
		outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	training_time_list = []
	feature_time_list = []
	permutation_time_list = []
	shap_time_list = []
	# Learning model for each Target Gene
	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		temp_GBM = time.time()
		GBM = GradientBoostingRegressor(random_state=RAND_SEED, learning_rate= GBM_KWARGS["learning_rate"], 
										n_estimators = GBM_KWARGS["n_estimators"], max_features = GBM_KWARGS["max_features"]) # args are from arboreto/core.py
		#GBM.fit(x_train, y_train, monitor=EarlyStopMonitor(25)) # the lenght of the early stoping window is the same as in arboreto
		GBM.fit(x_train, y_train)
		training_time_list.append(time.time() - temp_GBM)

		predictions = GBM.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))
		
		# Create labels for indexing evaluation metrics
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		if Comp_FI == True :
			temp_GBM = time.time()
			importances = GBM.feature_importances_
			FI_importances = pd.Series(importances, index=feature_names).sort_values(0, ascending = False)
			feature_time_list.append(time.time() - temp_GBM)

		if Comp_PI == True :
			temp_GBM = time.time()
			importances = permutation_importance(GBM, x_test, y_test, random_state = RAND_SEED, scoring = "r2")
			PI_importances = pd.Series(importances.importances_mean, index=feature_names).sort_values(0, ascending = False)
			permutation_time_list.append(time.time() - temp_GBM)

		temp_GBM = time.time()
		# Get Shapley values
		explainer = shap.Explainer(GBM)
		shap_val = np.array(explainer.shap_values(x_test))
		shap_vector = np.sum(abs(shap_val), axis = 0).T
		shap_time_list.append(time.time() - temp_GBM)
		
		# Writing result to file for evaluation
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				if Comp_FI == True :
					outFile2.write('\t'.join([gene_names[i],target_gene_name,str(FI_importances[j])])+'\n')
				if Comp_PI == True :
					outFile3.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances[j])])+'\n')
				j += 1

	total_time = time.time() - start_GBM
	training_time = sum(training_time_list)/len(training_time_list)
	feature_time = sum(feature_time_list)/len(feature_time_list)
	permutation_time = sum(permutation_time_list)/len(permutation_time_list)
	shap_time = sum(shap_time_list)/len(shap_time_list)

	with open("Time_log.txt", "a") as f :
		f.write("\n")
		f.write("Temps d'execution de GBM : %i \n" %(total_time))
		f.write("Temps d'apprentissage moyen pour GBM pour 1 gène : %i \n" %(training_time))
		f.write("Temps de calcul moyen du FI pour GBM pour 1 gène : %i \n" %(feature_time))
		f.write("Temps de calcul moyen du PI pour GBM pour 1 gène : %i \n" %(permutation_time))
		f.write("Temps de calcul moyen de SHAP pour GBM pour 1 gène : %i \n" %(shap_time))

	outFile.close()
	if Comp_FI == True :
		outFile2.close()
	if Comp_PI == True :
		outFile3.close()
	return r2_list

def compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, Comp_PI = True) :
	"""
	Function that compute Feature importance of target gene using Neural networks.
	Base : Compute Shapley value, if Comp_PI = True, compute permutation importance
	"""
	start_NN = time.time()

	# Initialize R2 scores
	r2_list_simple = []
	r2_list_complex = []

	# SHAP initializer
	NN_path = create_dir(path, "SH_NN", network, dataset)
	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	NN_path2 = create_dir(path, "SH_NND", network, dataset)
	outFile2 = open(NN_path2,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Permutation importance initializer
	if Comp_PI == True :
		NN_PI_path = create_dir(path, "PI_NN", network, dataset)
		outFile3 = open(NN_PI_path,'w')
		outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
		NN_PI_path2 = create_dir(path, "PI_NND", network, dataset)
		outFile4 = open(NN_PI_path2,'w')
		outFile4.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	training_time_list = []
	permutation_time_list = []
	shap_time_list = []
	trainingD_time_list = []
	permutationD_time_list = []
	shapD_time_list = []
	# Learning models for each Target Gene
	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Normalisation
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		################################## Simple model ##############################################
		temp_NN = time.time()
		dnn_scRNA_model = build_and_compile_nn_simple_model(RAND_SEED)
		
		dnn_scRNA_model.fit(
			x_train,
			y_train,
			epochs=100,
			# Suppress logging.
			verbose=0,
			batch_size = 32,
			# Early stopping
			callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20),
			# Calculate validation results on 10% of the training data.
			validation_split = 0.1)
		training_time_list.append(time.time() - temp_NN)
		
		predictions = dnn_scRNA_model.predict(x_test, verbose=0).flatten()
		r2_list_simple.append(r2_score(y_test, predictions))

		if Comp_PI == True :
			temp_NN = time.time()
			def score(X, y):
				y_pred = dnn_scRNA_model.predict(X, verbose = 0).flatten()
				return r2_score(y, y_pred)
			base_score, score_decreases = get_score_importances(score, x_test, y_test)
			PI_importances_simple = np.mean(score_decreases, axis=0)
			permutation_time_list.append(time.time() - temp_NN)

		temp_NN = time.time()		
		# Shapley value
		explainer_simple = shap.DeepExplainer(dnn_scRNA_model, x_train)
		shap_val_simple = np.array(explainer_simple.shap_values(x_test)[0])
		shap_vector_simple = np.sum(abs(shap_val_simple), axis = 0).T
		shap_time_list.append(time.time() - temp_NN)

		################################## Complex model ##############################################
		temp_NN = time.time()
		dnn_scRNA_model2 = build_and_compile_nn_complex_model(RAND_SEED)
		
		dnn_scRNA_model2.fit(
			x_train,
			y_train,
			epochs=100,
			# Suppress logging.
			verbose=0,
			batch_size = 32,
			# Early stopping
			callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20),
			# Calculate validation results on 10% of the training data.
			validation_split = 0.1)
		trainingD_time_list.append(time.time() - temp_NN)

		predictions = dnn_scRNA_model2.predict(x_test, verbose=0).flatten()
		r2_list_complex.append(r2_score(y_test, predictions))

		if Comp_PI == True :
			temp_NN = time.time()
			def score(X, y):
				y_pred = dnn_scRNA_model2.predict(X, verbose = 0).flatten()
				return r2_score(y, y_pred)
			base_score, score_decreases = get_score_importances(score, x_test, y_test)
			PI_importances_complex = np.mean(score_decreases, axis=0)
			permutationD_time_list.append(time.time() - temp_NN)
		
		temp_NN = time.time()
		# Shapley value
		explainer_complex = shap.DeepExplainer(dnn_scRNA_model2, x_train)
		shap_val_complex = np.array(explainer_complex.shap_values(x_test)[0])
		shap_vector_complex = np.sum(abs(shap_val_complex), axis = 0).T
		shapD_time_list.append(time.time() - temp_NN)

		
		# Writing result to file for evaluation
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_simple[j])])+'\n')
				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_complex[j])])+'\n')
				if Comp_PI == True :
					outFile3.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances_simple[j])])+'\n')
					outFile4.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances_complex[j])])+'\n')
				j += 1

	total_time = time.time() - start_NN
	training_time = sum(training_time_list)/len(training_time_list)
	permutation_time = sum(permutation_time_list)/len(permutation_time_list)
	shap_time = sum(shap_time_list)/len(shap_time_list)
	trainingD_time = sum(trainingD_time_list)/len(trainingD_time_list)
	permutationD_time = sum(permutationD_time_list)/len(permutationD_time_list)
	shapD_time = sum(shapD_time_list)/len(shapD_time_list)

	with open("Time_log.txt", "a") as f :
		f.write("\n")
		f.write("Temps d'execution des 2 NN séquentiels : %i \n" %(total_time))
		f.write("Temps d'apprentissage moyen pour NN pour 1 gène : %i \n" %(training_time))
		f.write("Temps de calcul moyen du PI pour NN pour 1 gène : %i \n" %(permutation_time))
		f.write("Temps de calcul moyen de SHAP pour NN pour 1 gène : %i \n" %(shap_time))
		f.write("Temps d'apprentissage moyen pour NND pour 1 gène : %i \n" %(trainingD_time))
		f.write("Temps de calcul moyen du PI pour NND pour 1 gène : %i \n" %(permutationD_time))
		f.write("Temps de calcul moyen de SHAP pour NND pour 1 gène : %i \n" %(shapD_time))

	outFile.close()
	outFile2.close()
	if Comp_PI == True :
		outFile3.close()
		outFile4.close()
	return r2_list_simple, r2_list_complex

def compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, Comp_PI = True, number = 3) :
	"""
	Function that compute Feature importance of target gene using 3 learning Neural networks that concatenate before prediction.
	Base : Compute Shapley value, if Comp_PI = True, compute permutation importance
	"""
	start_3NN = time.time()

	# Initialize R2 scores
	r2_list_separated = []
	r2_list_duplicated = []
	r2_list_permuted = []

	# SHAP initializer
	NN_path = create_dir(path, "SH_%iNND-Sep" %(number), network, dataset)
	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	NN_path2 = create_dir(path, "SH_%iNND-Dup" %(number), network, dataset)
	outFile2 = open(NN_path2,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	NN_path3 = create_dir(path, "SH_%iNND-Perm" %(number), network, dataset)
	outFile3 = open(NN_path3,'w')
	outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	# Permutation importance initializer
	if Comp_PI == True :
		NN_PI_path = create_dir(path, "PI_%iNND-Sep" %(number), network, dataset)
		outFile4 = open(NN_PI_path,'w')
		outFile4.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
		NN_PI_path2 = create_dir(path, "PI_%iNND-Dup" %(number), network, dataset)
		outFile5 = open(NN_PI_path2,'w')
		outFile5.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
		NN_PI_path3 = create_dir(path, "PI_%iNND-Perm" %(number), network, dataset)
		outFile6 = open(NN_PI_path3,'w')
		outFile6.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	training_time_list = []
	permutation_time_list = []
	shap_time_list = []
	trainingD_time_list = []
	permutationD_time_list = []
	shapD_time_list = []
	trainingP_time_list = []
	permutationP_time_list = []
	shapP_time_list = []
	# Learning models for each Target Gene
	for target_gene_index in tqdm(rand_index): 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)
		
		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		# Découpage du dataset en number morceaux
		if x_train.shape[0] % number != 0 :
			for i in range(1,number) :
				if x_train.shape[0] % number == i  :
					x_train = x_train[0:-i]
					y_train = y_train[0:-i]
					model_learn = np.split(x_train, number)
					y_learn = np.split(y_train, number)
					break
		else :
			model_learn = np.split(x_train, number)
			y_learn = np.split(y_train, number)
		
		################################## Separated model ##############################################
		temp_3NN = time.time()
		dnn_scRNA_model = build_and_compile_nn_functional_model(number, model_learn[0], RAND_SEED)

		dnn_scRNA_model.fit(
		model_learn,
		y_learn,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		training_time_list.append(time.time() - temp_3NN)
		
		if x_test.shape[0] % number != 0 :
			for i in range(1,number) :
				if x_test.shape[0] % number == i  :
					x_test = x_test[0:-i]
					y_test = y_test[0:-i]
					model_pred = np.split(x_test, number)
					y_pred = np.split(y_test, number)
					break
		else :
			model_pred = np.split(x_test, number)
			y_pred = np.split(y_test, number)
		
		pred = dnn_scRNA_model.predict(model_pred, verbose = 0).T
		predictions = list(itertools.chain.from_iterable(pred))
		r2_list_separated.append(r2_score(y_test, predictions))

		if Comp_PI == True :
			def score(X, y):
				if X.shape[0] % number != 0 :
					for i in range(1,number) :
						if X.shape[0] % number == i  :
							X = X[0:-i]
							model_pred = np.split(X, number)
							break
				else :
					model_pred = np.split(X, number)
				pred = dnn_scRNA_model.predict(model_pred, verbose = 0).T
				y_pred = list(itertools.chain.from_iterable(pred))
				return r2_score(y, y_pred)
			base_score, score_decreases = get_score_importances(score, x_test, y_test)
			PI_importances_separated = np.mean(score_decreases, axis=0)
			permutation_time_list.append(time.time() - temp_3NN)
		
		temp_3NN = time.time()

		# Shapley values
		explainer = shap.DeepExplainer(dnn_scRNA_model, model_learn)
		shap_val = explainer.shap_values(model_pred)
		
		shap_val_test = []
		for i in range(number) : # On somme les contributions de chaque modèle pour 1 prédiction
			shap_val_test.append(sum(shap_val[i])/len(shap_val[i]))

		# On reconstruit le vecteur avec toutes les cellules
		shap_val = np.array(list(itertools.chain.from_iterable(shap_val_test))) # ici tout est à la suite donc c'est facile
		shap_vector_separated = np.sum(abs(shap_val), axis = 0).T
		shap_time_list.append(time.time() - temp_3NN)
		
		################################## Duplicated model ##############################################
		# Create the duplicated vector
		dupl_train = []
		dupl_test = []
		ydupl_train = []
		for i in range(number) :
			dupl_train.append(x_train)
			dupl_test.append(x_test)
			ydupl_train.append(y_train)

		temp_3NN = time.time()
		dnn_scRNA_model2 = build_and_compile_nn_functional_model(number, x_train, RAND_SEED)

		dnn_scRNA_model2.fit(
		dupl_train,
		ydupl_train,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		trainingD_time_list.append(time.time() - temp_3NN)
		
		pred = dnn_scRNA_model2.predict(dupl_test, verbose = 0)
		predictions = (np.sum(pred, axis = 1))/len(pred[0])
		r2_list_duplicated.append(r2_score(y_test, predictions))

		if Comp_PI == True :
			temp_3NN = time.time()
			def score(X, y):
				dupl_test = np.split(X, number)
				pred = dnn_scRNA_model2.predict(dupl_test, verbose = 0)
				y_pred = (np.sum(pred, axis = 1))/len(pred[0])
				return r2_score(y, y_pred)
			x_test_all = np.array(list(itertools.chain.from_iterable(dupl_test)))
			base_score, score_decreases = get_score_importances(score, x_test_all, y_test)
			PI_importances_duplicated = np.mean(score_decreases, axis=0)
			permutationD_time_list.append(time.time() - temp_3NN)
		
		temp_3NN = time.time()
		# Shapley values
		explainer = shap.DeepExplainer(dnn_scRNA_model2, dupl_train)
		shap_val = np.array(explainer.shap_values(dupl_test))
		
		# On fait la moyenne
		shap_val = sum(shap_val)/len(shap_val) # Pour chaque sortie 
		shap_val = sum(shap_val)/len(shap_val) # Pour chaque modèle
		shap_vector_duplicated = np.sum(abs(shap_val), axis = 0).T
		shapD_time_list.append(time.time() - temp_3NN)

		################################## Permuted model ##############################################
		
		# On crée les combinaisons
		perm_learn = list(itertools.permutations(model_learn))
		perm_y = list(itertools.permutations(y_learn))

		permuted_learn = [[] for x in range(number)]
		permuted_y = [[] for x in range(number)]
		for i in range(number) :
			for j in range(len(perm_learn)) : # même découpage donc ok
				permuted_learn[i].append(perm_learn[j][i])
				permuted_y[i].append(perm_y[j][i])
			permuted_learn[i] = np.array(list(itertools.chain.from_iterable(permuted_learn[i])))
			permuted_y[i] = np.array(list(itertools.chain.from_iterable(permuted_y[i])))

		temp_3NN = time.time()
		dnn_scRNA_model3 = build_and_compile_nn_functional_model(number, permuted_learn[0], RAND_SEED)
		
		dnn_scRNA_model3.fit(
		permuted_learn,
		permuted_y,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		trainingP_time_list.append(time.time() - temp_3NN)
		
		perm_pred = list(itertools.permutations(model_pred))
		perm_ypred = list(itertools.permutations(y_pred))

		permuted_pred = [[] for x in range(number)]
		permuted_ypred = [[] for x in range(number)]
		for i in range(number) :
			for j in range(len(perm_pred)) :
				permuted_pred[i].append(perm_pred[j][i])
				permuted_ypred[i].append(perm_ypred[j][i])
			permuted_pred[i] = np.array(list(itertools.chain.from_iterable(permuted_pred[i])))
			permuted_ypred[i] = np.array(list(itertools.chain.from_iterable(permuted_ypred[i])))

		pred = dnn_scRNA_model3.predict(permuted_pred, verbose = 0)

		predictions = list(itertools.chain.from_iterable(pred))
		y_line = list(itertools.chain.from_iterable(permuted_ypred))

		r2_list_permuted.append(r2_score(y_line, predictions))

		if Comp_PI == True :
			x_tot = np.array(list(itertools.chain.from_iterable(permuted_pred)))
			temp_3NN = time.time()
			def score(X, y):
				permuted_pred = np.split(X, number)
				pred = dnn_scRNA_model3.predict(permuted_pred, verbose = 0)
				y_pred = list(itertools.chain.from_iterable(pred))
				return r2_score(y, y_pred)
			base_score, score_decreases = get_score_importances(score, x_tot, y_line)
			PI_importances_permuted = np.mean(score_decreases, axis=0)
			permutationP_time_list.append(time.time() - temp_3NN)
		
		temp_3NN = time.time()
		# Shapley values
		permuted_raisonnable_train = []
		permuted_raisonnable_pred = []
		for i in range(number) :
			permuted_raisonnable_pred.append(permuted_pred[i][0:len(x_test)])
			permuted_raisonnable_train.append(permuted_learn[i][0:len(x_train)])
		
		explainer = shap.DeepExplainer(dnn_scRNA_model3, permuted_raisonnable_train)
		shap_val = explainer.shap_values(permuted_raisonnable_pred)
		
		shap_val_test = []
		for i in range(number) : # On somme les contributions de chaque modèle pour 1 prédiction
			shap_val_test.append(sum(shap_val[i])/len(shap_val[i]))

		# On reconstruit le vecteur avec toutes les cellules
		shap_val= np.array(list(itertools.chain.from_iterable(shap_val_test))) # finalement on s'en fout de les mettre dans l'ordre puisque l'on va tout sommer

		shap_vector_permuted = np.sum(abs(shap_val), axis = 0).T
		shapP_time_list.append(time.time() - temp_3NN)

		# Writing result to file for evaluation
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_separated[j])])+'\n')
				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_duplicated[j])])+'\n')
				outFile3.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_permuted[j])])+'\n')
				if Comp_PI == True :
					outFile4.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances_separated[j])])+'\n')
					outFile5.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances_duplicated[j])])+'\n')
					outFile6.write('\t'.join([gene_names[i],target_gene_name,str(PI_importances_permuted[j])])+'\n')
				j += 1

	total_time = time.time() - start_3NN
	training_time = sum(training_time_list)/len(training_time_list)
	permutation_time = sum(permutation_time_list)/len(permutation_time_list)
	shap_time = sum(shap_time_list)/len(shap_time_list)
	trainingD_time = sum(trainingD_time_list)/len(trainingD_time_list)
	permutationD_time = sum(permutationD_time_list)/len(permutationD_time_list)
	shapD_time = sum(shapD_time_list)/len(shapD_time_list)
	trainingP_time = sum(trainingP_time_list)/len(trainingP_time_list)
	permutationP_time = sum(permutationP_time_list)/len(permutationP_time_list)
	shapP_time = sum(shapP_time_list)/len(shapP_time_list)

	with open("Time_log.txt", "a") as f :
		f.write("\n")
		f.write("Temps d'execution des 3 3NN functionnels : %i \n" %(total_time))
		f.write("Temps d'apprentissage moyen pour 3NN-Sep pour 1 gène : %i \n" %(training_time))
		f.write("Temps de calcul moyen du PI pour 3NN-Sep pour 1 gène : %i \n" %(permutation_time))
		f.write("Temps de calcul moyen de SHAP pour 3NN-Sep pour 1 gène : %i \n" %(shap_time))
		f.write("Temps d'apprentissage moyen pour 3NN-Dup pour 1 gène : %i \n" %(trainingD_time))
		f.write("Temps de calcul moyen du PI pour 3NN-Dup pour 1 gène : %i \n" %(permutationD_time))
		f.write("Temps de calcul moyen de SHAP pour 3NN-Dup pour 1 gène : %i \n" %(shapD_time))
		f.write("Temps d'apprentissage moyen pour 3NN-Perm pour 1 gène : %i \n" %(trainingP_time))
		f.write("Temps de calcul moyen du PI pour 3NN-Perm pour 1 gène : %i \n" %(permutationP_time))
		f.write("Temps de calcul moyen de SHAP pour 3NN-Perm pour 1 gène : %i \n" %(shapP_time))

	outFile.close()
	outFile2.close()
	outFile3.close()
	if Comp_PI == True :
		outFile4.close()
		outFile5.close()
		outFile6.close()
	return r2_list_separated, r2_list_duplicated, r2_list_permuted