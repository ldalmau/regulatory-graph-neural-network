import seaborn as sns
from pathlib import Path
sns.set(rc={"lines.linewidth": 2}, palette  = "deep", style = "ticks")
from sklearn.metrics import precision_recall_curve, roc_curve, auc
from itertools import product, permutations, combinations, combinations_with_replacement
from tqdm import tqdm
import argparse
import networkx as nx
import multiprocessing
from collections import defaultdict
from multiprocessing import Pool, cpu_count
from networkx.convert_matrix import from_pandas_adjacency
import yaml
import itertools

import os
import shutil
import time
import pandas as pd
import numpy as np

# 	Evaluation of outputs
# 		AUROC AUPRC
def PRROC(true_path, compute_path, directed = False, selfEdges = False):
	'''
	Computes areas under the precision-recall and ROC curves
	for a given dataset for each algorithm.
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param directed:   A flag to indicate whether to treat predictionsas directed edges (directed = True) or undirected edges (directed = False).
	:type directed: bool
	
	:param selfEdges:   A flag to indicate whether to includeself-edges (selfEdges = True) or exclude self-edges (selfEdges = False) from evaluation.
	:type selfEdges: bool
	
	:param plotFlag:   A flag to indicate whether or not to save PR and ROC plots.
	:type plotFlag: bool
	
	:returns:
			- AUPRC: A dictionary containing AUPRC values for each algorithm
			- AUROC: A dictionary containing AUROC values for each algorithm
	'''
	
	# Read file for trueEdges
	trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"),sep = ',', 
								header = 0, index_col = None)
	
	# Initialize data list
	precisionDict = []
	recallDict = []
	FPRDict = []
	TPRDict = []
	AUPRC = []
	AUROC = []
	
	if directed:
		# check if the output rankedEdges file exists
		if Path(compute_path +'/rankedEdges.csv').exists():
			# Initialize Precsion
			predDF = pd.read_csv(compute_path + '/rankedEdges.csv', sep = '\t', header =  0, index_col = None)
			precisionDict, recallDict, FPRDict, TPRDict, AUPRC, AUROC = computeScores(trueEdgesDF, predDF, directed = True, selfEdges = selfEdges)
		else:
			print(compute_path +'/rankedEdges.csv', ' does not exist. Skipping...')
			PRName = '/PRplot'
			ROCName = '/ROCplot'
	else:
		# check if the output rankedEdges file exists
		if Path(compute_path + '/rankedEdges.csv').exists():
			# Initialize Precsion
			predDF = pd.read_csv(compute_path + '/rankedEdges.csv', sep = '\t', header =  0, index_col = None)
			precisionDict, recallDict, FPRDict, TPRDict, AUPRC, AUROC = computeScores(trueEdgesDF, predDF, directed = False, selfEdges = selfEdges)
		else:
			print(compute_path + '/rankedEdges.csv', ' does not exist. Skipping...')
			PRName = '/uPRplot'
			ROCName = '/uROCplot'

	return AUPRC, AUROC

def computeScores(trueEdgesDF, predEdgeDF, directed = True, selfEdges = True):
	'''        
	Computes precision-recall and ROC curves
	using scikit-learn for a given set of predictions in the 
	form of a DataFrame.
	Retrieve from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param trueEdgesDF:   A pandas dataframe containing the true classes.The indices of this dataframe are all possible edgesin a graph formed using the genes in the given dataset. This dataframe only has one column to indicate the classlabel of an edge. If an edge is present in the reference network, it gets a class label of 1, else 0.
	:type trueEdgesDF: DataFrame
	
	:param predEdgeDF:   A pandas dataframe containing the edge ranks from the prediced network. The indices of this dataframe are all possible edges.This dataframe only has one column to indicate the edge weightsin the predicted network. Higher the weight, higher the edge confidence.
	:type predEdgeDF: DataFrame
	
	:param directed:   A flag to indicate whether to treat predictionsas directed edges (directed = True) or undirected edges (directed = False).
	:type directed: bool
	:param selfEdges:   A flag to indicate whether to includeself-edges (selfEdges = True) or exclude self-edges (selfEdges = False) from evaluation.
	:type selfEdges: bool
	
	:returns:
			- prec: A list of precision values (for PR plot)
			- recall: A list of precision values (for PR plot)
			- fpr: A list of false positive rates (for ROC plot)
			- tpr: A list of true positive rates (for ROC plot)
			- AUPRC: Area under the precision-recall curve
			- AUROC: Area under the ROC curve
	'''
	if directed:        
		# Initialize dictionaries with all 
		# possible edges
		if selfEdges:
			possibleEdges = list(product(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), repeat = 2))
		else:
			possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		
		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth
		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[(trueEdgesDF['Gene1'] == key.split('|')[0]) &
					(trueEdgesDF['Gene2'] == key.split('|')[1])])>0:
					TrueEdgeDict[key] = 1
		
		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[(predEdgeDF['Gene1'] == key.split('|')[0]) &
								(predEdgeDF['Gene2'] == key.split('|')[1])]
		if len(subDF)>0:
			PredEdgeDict[key] = np.abs(subDF.EdgeWeight.values[0])

	# if undirected
	else:
		# Initialize dictionaries with all 
		# possible edges
		if selfEdges:
			possibleEdges = list(combinations_with_replacement(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		else:
			possibleEdges = list(combinations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth

		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[((trueEdgesDF['Gene1'] == key.split('|')[0]) &
							(trueEdgesDF['Gene2'] == key.split('|')[1])) |
								((trueEdgesDF['Gene2'] == key.split('|')[0]) &
							(trueEdgesDF['Gene1'] == key.split('|')[1]))]) > 0:
				TrueEdgeDict[key] = 1  

		# Compute PredEdgeDict Dictionary
		# from predEdgeDF

		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[((predEdgeDF['Gene1'] == key.split('|')[0]) &
								(predEdgeDF['Gene2'] == key.split('|')[1])) |
								((predEdgeDF['Gene2'] == key.split('|')[0]) &
								(predEdgeDF['Gene1'] == key.split('|')[1]))]
			if len(subDF)>0:
				PredEdgeDict[key] = max(np.abs(subDF.EdgeWeight.values))

		
		
	# Combine into one dataframe
	# to pass it to sklearn
	outDF = pd.DataFrame([TrueEdgeDict,PredEdgeDict]).T
	outDF.columns = ['TrueEdges','PredEdges']
	
	fpr, tpr, thresholds = roc_curve(y_true=outDF['TrueEdges'], y_score=outDF['PredEdges'], pos_label=1)

	prec, recall, thresholds = precision_recall_curve(y_true=outDF['TrueEdges'], probas_pred=outDF['PredEdges'], pos_label=1)
	
	return prec, recall, fpr, tpr, auc(recall, prec), auc(fpr, tpr)

#		EarlyPrecision
def EarlyPrec(true_path, compute_path, TFEdges = False):
	'''
	Computes early precision for a given algorithm for each dataset.
	We define early precision as the fraction of true 
	positives in the top-k edges, where k is the number of
	edges in the ground truth network (excluding self loops).
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline

	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: BLEval
	  
	:param algorithmName: Name of the algorithm for which the early precision is computed.
	:type algorithmName: str
	

	:returns:
		A dataframe containing early precision values
		for a given algorithm for each dataset.
	'''
	rankDict = []
	trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',
					header = 0, index_col = None)
	trueEdgesDF = trueEdgesDF.loc[(trueEdgesDF['Gene1'] != trueEdgesDF['Gene2'])]
	trueEdgesDF.drop_duplicates(keep = 'first', inplace=True)
	trueEdgesDF.reset_index(drop=True, inplace=True)


	rank_path = compute_path + "/rankedEdges.csv"
	if not os.path.isdir(compute_path):
		rankDict = set([])
	try:
		predDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
	except:
		print("\nSkipping early precision computation for ", algo, "on path", compute_path)
		rankDict = set([])

	predDF = predDF.loc[(predDF['Gene1'] != predDF['Gene2'])]
	predDF.drop_duplicates(keep = 'first', inplace=True)
	predDF.reset_index(drop=True, inplace=True)
	
	if TFEdges:
		# Consider only edges going out of TFs
		
		# Get a list of all possible TF to gene interactions 
		uniqueNodes = np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']])
		possibleEdges_TF = set(product(set(trueEdgesDF.Gene1),set(uniqueNodes)))

		# Get a list of all possible interactions 
		possibleEdges_noSelf = set(permutations(uniqueNodes, r = 2))
		
		# Find intersection of above lists to ignore self edges
		# TODO: is there a better way of doing this?
		possibleEdges = possibleEdges_TF.intersection(possibleEdges_noSelf)
		
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		trueEdges = trueEdgesDF['Gene1'] + "|" + trueEdgesDF['Gene2']
		trueEdges = trueEdges[trueEdges.isin(TrueEdgeDict)]
		print("\nEdges considered ", len(trueEdges))
		numEdges = len(trueEdges)
		 
		predDF['Edges'] = predDF['Gene1'] + "|" + predDF['Gene2']
		# limit the predicted edges to the genes that are in the ground truth
		predDF = predDF[predDF['Edges'].isin(TrueEdgeDict)]

	else:
		trueEdges = trueEdgesDF['Gene1'] + "|" + trueEdgesDF['Gene2']
		trueEdges = set(trueEdges.values)
		numEdges = len(trueEdges)
		
		# check if ranked edges list is empty
		# if so, it is just set to an empty set

	if not predDF.shape[0] == 0:

		# we want to ensure that we do not include
		# edges without any edge weight
		# so check if the non-zero minimum is
		# greater than the edge weight of the top-kth
		# node, else use the non-zero minimum value.
		predDF.EdgeWeight = predDF.EdgeWeight.round(6)
		predDF.EdgeWeight = predDF.EdgeWeight.abs()

		# Use num True edges or the number of
		# edges in the dataframe, which ever is lower
		maxk = min(predDF.shape[0], numEdges)
		edgeWeightTopk = predDF.iloc[maxk-1].EdgeWeight

		nonZeroMin = np.nanmin(predDF.EdgeWeight.replace(0, np.nan).values)
		bestVal = max(nonZeroMin, edgeWeightTopk)

		newDF = predDF.loc[(predDF['EdgeWeight'] >= bestVal)]
		rankDict = set(newDF['Gene1'] + "|" + newDF['Gene2'])
	else:
		print("\nSkipping early precision computation for on path ", rank_path,"due to lack of predictions.")
		rankDict = set([])
	Eprec = []
	Erec = []
	if len(rankDict) != 0:
		intersectionSet = rankDict.intersection(trueEdges)
		Eprec = len(intersectionSet)/len(rankDict)
		Erec = len(intersectionSet)/len(trueEdges)
	else:
		Eprec = 0
		Erec = 0

	return(Eprec)

#		Jaccard
def Jaccard(true_path, compute_path, dataset):
	"""
	A function to compute median pairwirse Jaccard similarity index
	of predicted top-k edges for a given set of datasets (obtained from
	the same reference network). Here k is the number of edges in the
	reference network (excluding self loops). 
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: :obj:`BLEval`
	
	
	:param algorithmName: Name of the algorithm for which the Spearman correlation is computed.
	:type algorithmName: str
	
	
	:returns:
		- median: Median of Jaccard correlation values
		- mad: Median Absolute Deviation of  the Spearman correlation values
	"""

	rankDict = {}
	sim_names = []
	for dat in dataset:
		trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',header = 0, index_col = None)

		possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))

		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth
		numEdges = 0
		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[(trueEdgesDF['Gene1'] == key.split('|')[0]) &
					(trueEdgesDF['Gene2'] == key.split('|')[1])])>0:
				TrueEdgeDict[key] = 1
				numEdges += 1

		outDir = os.path.join(compute_path, dat)
		rank_path = os.path.join(outDir, "rankedEdges.csv")
		if not os.path.isdir(outDir):
			continue
		try:
			predDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
		except:
			print("Skipping Jaccard computation for ", algorithmName, "on path", outDir)
			continue

		predDF = predDF.loc[(predDF['Gene1'] != predDF['Gene2'])]
		predDF.drop_duplicates(keep = 'first', inplace=True)
		predDF.reset_index(drop = True,  inplace= True)
		# check if ranked edges list is empty
		# if so, it is just set to an empty set

		if not predDF.shape[0] == 0:

			# we want to ensure that we do not include
			# edges without any edge weight
			# so check if the non-zero minimum is
			# greater than the edge weight of the top-kth
			# node, else use the non-zero minimum value.
			predDF.EdgeWeight = predDF.EdgeWeight.round(6)
			predDF.EdgeWeight = predDF.EdgeWeight.abs()

			# Use num True edges or the number of
			# edges in the dataframe, which ever is lower
			maxk = min(predDF.shape[0], numEdges)
			edgeWeightTopk = predDF.iloc[maxk-1].EdgeWeight

			nonZeroMin = np.nanmin(predDF.EdgeWeight.replace(0, np.nan).values)
			bestVal = max(nonZeroMin, edgeWeightTopk)

			newDF = predDF.loc[(predDF['EdgeWeight'] >= bestVal)]
			rankDict[dat] = set(newDF['Gene1'] + "|" + newDF['Gene2'])
		else:
			rankDict[dat] = set([])

	Jdf = computePairwiseJacc(rankDict)
	df = Jdf.where(np.triu(np.ones(Jdf.shape),  k = 1).astype(np.bool))
	df = df.stack().reset_index()
	df.columns = ['Row','Column','Value']
	return(df.Value.median(),df.Value.mad())


def computePairwiseJacc(inDict):
	"""
	A helper function to compute all pairwise Jaccard similarity indices
	of predicted top-k edges for a given set of datasets (obtained from
	the same reference network). Here k is the number of edges in the
	reference network (excluding self loops). 
	Retrevied from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	
	:param inDict:  A dictionary contaninig top-k predicted edges  for each dataset. Here, keys are the dataset name and the values are the set of top-k edges.
	:type inDict: dict
	:returns:
		A dataframe containing pairwise Jaccard similarity index values
	"""
	jaccDF = {key:{key1:{} for key1 in inDict.keys()} for key in inDict.keys()}
	for key_i in inDict.keys():
		for key_j in inDict.keys():
			num = len(inDict[key_i].intersection(inDict[key_j]))
			den = len(inDict[key_i].union(inDict[key_j]))
			if den != 0:
				jaccDF[key_i][key_j] = num/den
			else:
				jaccDF[key_i][key_j] = 0
	return pd.DataFrame(jaccDF)

#		Spearman
def Spearman(true_path, compute_path, dataset):
	"""
	A function to compute median pairwirse Spearman correlation
	of predicted ranked edges, i.e., the outputs of different datasets
	generated from the same reference network, for a given algorithm.
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: BLEval
	
	:param algorithmName: Name of the algorithm for which the Spearman correlation is computed.
	:type algorithmName: str
	
	
	:returns:
		- median: Median of Spearman correlation values
		- mad: Median Absolute Deviation of  the Spearman correlation values
	"""

	rankDict = {}
	sim_names = []
	for dat in dataset:
		trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',
						header = 0, index_col = None)
		possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		outDir = os.path.join(compute_path, dat)
		rank_path = os.path.join(outDir, "rankedEdges.csv")
		if not os.path.isdir(outDir):
			continue
		try:
			predEdgeDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
		except:
			print("Skipping spearman computation for ", algorithmName, "on path", outDir)
			continue

		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[(predEdgeDF['Gene1'] == key.split('|')[0]) &
							(predEdgeDF['Gene2'] == key.split('|')[1])]
			if len(subDF)>0:
				PredEdgeDict[key] = np.abs(subDF.EdgeWeight.values[0])
		rankDict[dat] = PredEdgeDict
		sim_names.append(dataset)
	df2 = pd.DataFrame.from_dict(rankDict)
	spearmanDF = df2.corr(method='spearman')


	df = spearmanDF.where(np.triu(np.ones(spearmanDF.shape),  k = 1).astype(np.bool))

	df = df.stack().reset_index()
	df.columns = ['Row','Column','Value']

	return(df.Value.median(),df.Value.mad())