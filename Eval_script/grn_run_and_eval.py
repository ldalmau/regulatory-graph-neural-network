# Import
# 	Run
import random
#import copy
#from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor, ExtraTreesRegressor
#from sklearn.model_selection import train_test_split
#from sklearn.metrics import mean_squared_error, explained_variance_score, mean_absolute_error, r2_score, mean_absolute_percentage_error
import tools_lisa 
from tools_lisa import prepare_data, prepare_for_regressor, parallel_plot
import tensorflow as tf
#from tensorflow import keras
#from tensorflow.keras import layers
#from tensorflow.keras import regularizers
import numpy as np
#import shap
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg') # Disable display, necessary due to WSL2 (Windows Subsystem for Linux) that can't open Linux windows

tf.get_logger().setLevel('ERROR') # Suppress a warning that is often display but not relevant

# 	Eval
import seaborn as sns
from pathlib import Path
sns.set(rc={"lines.linewidth": 2}, palette  = "deep", style = "ticks")
#from sklearn.metrics import precision_recall_curve, roc_curve, auc
#from itertools import product, permutations, combinations, combinations_with_replacement
from tqdm import tqdm
#import argparse
#import networkx as nx
#import multiprocessing
#from collections import defaultdict
#from multiprocessing import Pool, cpu_count
#from networkx.convert_matrix import from_pandas_adjacency
#import yaml
#import itertools

import os
import shutil
import argparse

import grn_func

# Search for args
input_parser = argparse.ArgumentParser()
input_parser.add_argument("method", help="Algorithm to train and test (could be : base, NN or Norm)")
args = input_parser.parse_args()

if args.method == "base" or args.method == "NN" or args.method == "Norm" or args.method == "Perm" :
	script_method = args.method
else :
	script_method = base

# Set seed
RAND_SEED = 744 #Usefull to reproduct the results, the value can be changed
random.seed(RAND_SEED)

# Set parameters
#	Name for lisa_tools use
DATA_TYPE="BEELINE"
#	TF_file, not usefull for beeeline synthetic dataset
TF_PATH="all"
# 	scikit-learn random forest regressor
RF_KWARGS = {
    'n_jobs': 1,
    'n_estimators': 10,
    'max_features': 'sqrt'
}

# 	scikit-learn gradient boosting regressor
GBM_KWARGS = {
    'learning_rate': 0.01,
    'n_estimators': 500,
    'max_features': 0.1
}


# Initialise env :
root = os.getcwd()
if not Path(root + "/data").exists() :
	print("'data' folder missing, please create the folder and fill it with datasets")

root_data = os.path.join(root, "data")

if not Path(root + "/algo").exists() :
	os.mkdir(os.path.join(root, "algo"))

root_algo = os.path.join(root, "algo")

if not Path(root + "/output").exists() :
	os.mkdir(os.path.join(root, "output"))

root_out = os.path.join(root, "output")

# Main 

networks = os.listdir(root_data)

network_bar = tqdm(networks)
all_R2 = []


# Cleanup
shutil.rmtree(root_algo)
os.mkdir(root_algo)

for net in network_bar :
	network_bar.set_description("Processing %s network" %(net))

	net_path = os.path.join(root_data, net)
	datasets = os.listdir(net_path)

	dataset_bar = tqdm(datasets)

	net_R2 = []

	for dat in dataset_bar :
		dataset_bar.set_description("Processing %s dataset" %(dat))

		dat_path = os.path.join(net_path, dat)
		expr_path = os.path.join(dat_path, "ExpressionData.csv")
		N_iter_regressors = grn_func.find__iter(expr_path)

		# Train

		gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, ordering_criterion, var_exp_list = prepare_data(DATA_TYPE, expr_path, TF_PATH, N_iter_regressors)
		
		if script_method == "base" :
			# Set de base :
			r2_NN = grn_func.compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_NN_fun = grn_func.compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_RF = grn_func.compute_RF(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, RF_KWARGS)
			r2_GBM = grn_func.compute_GBM(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, GBM_KWARGS)

			net_R2.append(np.array([sum(r2_NN)/len(r2_NN), sum(r2_NN_fun)/len(r2_NN_fun), sum(r2_RF)/len(r2_RF), sum(r2_GBM)/len(r2_GBM)]))
		elif script_method == "NN" :
			# Set NN comparaison
			r2_NN = grn_func.compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, shap_expl = "both")
			r2_NN_fun = grn_func.compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_NN_fun_same = grn_func.compute_fun_same_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_NN_fun_all = grn_func.compute_fun_all_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)

			net_R2.append(np.array([sum(r2_NN)/len(r2_NN), sum(r2_NN_fun)/len(r2_NN_fun), sum(r2_NN_fun_same)/len(r2_NN_fun_same), sum(r2_NN_fun_all)/len(r2_NN_fun_all)]))

		elif script_method == "Norm":
			# Set Norm comparaison
			r2_NN = grn_func.compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_NN_norm = grn_func.compute_NN_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_RF = grn_func.compute_RF(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, RF_KWARGS)
			r2_RF_norm = grn_func.compute_RF_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, RF_KWARGS)
			r2_GBM = grn_func.compute_GBM(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, GBM_KWARGS)
			r2_GBM_norm = grn_func.compute_GBM_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, GBM_KWARGS)
				
			net_R2.append(np.array([sum(r2_NN)/len(r2_NN), sum(r2_NN_norm)/len(r2_NN_norm), sum(r2_RF)/len(r2_RF), sum(r2_RF_norm)/len(r2_RF_norm), sum(r2_GBM)/len(r2_GBM), sum(r2_GBM_norm)/len(r2_GBM_norm)]))			

		elif script_method == "Perm":
			r2_NN = grn_func.compute_NN_FI(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			net_R2.append(np.array([sum(r2_NN)/len(r2_NN)]))

	all_R2.append([net, sum(net_R2)/len(net_R2)])

#print(all_R2)

print("Training complete")

# Compute AUROC, AUPRC and EalryPrecision + Jaccard and Spearman

algorithm = os.listdir(root_algo)
algorithm.sort()

network_bar = tqdm(networks)
for net in networks : # We evaluate for each network
	network_bar.set_description("Evaluating %s network" %(net))

	precision_dataset = pd.DataFrame(np.zeros((6, len(algorithm))), index = ["R²", "AUROC", "AUPRC", "EarlPrec","Jacc", "Spear"], columns = algorithm)
	can_compute_Jac = True

	
	# Set R²
	for i in range(len(all_R2)) :
		if net == all_R2[i][0] :
			r2_heatmap = all_R2[i][1]
	
	if script_method == "base" :
		#Set de base
		precision_dataset["NN"]["R²"] = r2_heatmap[0]
		precision_dataset["FI_NN"]["R²"] = r2_heatmap[0]
		precision_dataset["NN_fun"]["R²"] = r2_heatmap[1]
		precision_dataset["RF"]["R²"] = r2_heatmap[2]
		precision_dataset["FI_RF"]["R²"] = r2_heatmap[2]
		precision_dataset["GBM"]["R²"] = r2_heatmap[3]
		precision_dataset["FI_GBM"]["R²"] = r2_heatmap[3]

	elif script_method == "NN" :
		# Set NN comparaison
		precision_dataset["NN"]["R²"] = r2_heatmap[0]
		precision_dataset["FI_NN"]["R²"] = r2_heatmap[0]
		precision_dataset["NN_Ker"]["R²"] = r2_heatmap[0]
		precision_dataset["NN_fun"]["R²"] = r2_heatmap[1]
		precision_dataset["NN_fun_same"]["R²"] = r2_heatmap[2]
		precision_dataset["NN_fun_all"]["R²"] = r2_heatmap[3]

	elif script_method == "Norm" :
		# Set Norm comparaison
		precision_dataset["NN"]["R²"] = r2_heatmap[0]
		precision_dataset["FI_NN"]["R²"] = r2_heatmap[0]
		precision_dataset["NN_norm"]["R²"] = r2_heatmap[1]
		precision_dataset["RF"]["R²"] = r2_heatmap[2]
		precision_dataset["FI_RF"]["R²"] = r2_heatmap[2]
		precision_dataset["RF_norm"]["R²"] = r2_heatmap[3]
		precision_dataset["GBM"]["R²"] = r2_heatmap[4]
		precision_dataset["FI_GBM"]["R²"] = r2_heatmap[4]
		precision_dataset["GBM_norm"]["R²"] = r2_heatmap[5]


	for algo in algorithm : # and for all algorithm
		search_dir = os.path.join(root_algo, algo, net)
		num_data = os.listdir(search_dir)
		if len(num_data) == 1 : # check if we have 1 or more dataset
			true_path = os.path.join(root_data, net, num_data[0])
			compute_path = os.path.join(root_algo, algo, net, num_data[0])
			AUPRC, AUROC = grn_func.PRROC(true_path, compute_path, selfEdges = False)
			EarlyPrecision = grn_func.EarlyPrec(true_path, compute_path, TFEdges = False)
			print("Cannot compute Jaccard and Spearman for ", net, " , need more dataset to evaluate")
			precision_dataset[algo]["AUROC"] = AUROC
			precision_dataset[algo]["AUPRC"] = AUPRC
			precision_dataset[algo]["EarlPrec"] = EarlyPrecision
			can_compute_Jac = False
			if script_method == "Perm":
				precision_dataset[algo]["R²"] = all_R2[0][1]

		else :
			aur_list = []
			aup_list = []
			ear_list = []
			for i in range(len(num_data)) :
				true_path = os.path.join(root_data, net, num_data[i])
				compute_path = os.path.join(root_algo, algo, net, num_data[i])
				AUPRC, AUROC = grn_func.PRROC(true_path, compute_path, directed = False, selfEdges = False)
				EarlyPrecision = grn_func.EarlyPrec(true_path, compute_path, TFEdges = False)
				aur_list.append(AUROC)
				aup_list.append(AUPRC)
				ear_list.append(EarlyPrecision)
			
			precision_dataset[algo]["AUROC"] = sum(aur_list)/len(aur_list)
			precision_dataset[algo]["AUPRC"] = sum(aup_list)/len(aup_list)
			precision_dataset[algo]["EarlPrec"] = sum(ear_list)/len(ear_list)
			# Compute Jaccard / Spearman
			true_path = os.path.join(root_data, net, num_data[0])
			compute_path = os.path.join(root_algo, algo, net)
			Jacc_med, Jacc_mad = grn_func.Jaccard(true_path, compute_path, num_data)
			Spear_med, Spear_mad = grn_func.Spearman(true_path, compute_path, num_data)
			precision_dataset[algo]["Jacc"] = Jacc_med
			precision_dataset[algo]["Spear"] = Spear_med
			if script_method == "Perm":
				precision_dataset[algo]["R²"] = all_R2[0][1]

	if can_compute_Jac == False :
		precision_dataset.drop(["Jacc", "Spear"])

	# 1st : Write to file
	filename = "all_" + net + ".csv"
	precision_dataset.to_csv(os.path.join(root_out, filename))

	# 2nd : Heatmap
	plt.figure()
	sns.heatmap(precision_dataset, annot=True, vmin = 0, vmax = 1)
	filename = "heat_all_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))