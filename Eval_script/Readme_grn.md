** How to use grn_run_and_eval.py :

1. Setup a new conda environnement with the environnement.yml

`conda env create -f environment.yml`

and activate it

`conda activate tensorflow-env`

2. In the same folder as grn_run_and_eval.py, create a new folder "data" wich will contain the datasets to test

3. Add datasets in the "data" folder.
Beware, the dataset folder architecture should look like :

---| data (not part of the dataset)

------| network1 (a network to test)

---------| dataset1 (a dataset link to network1)

------------| ExpressionData.csv (table of the expression of dataset1)

------------| PseudoTime.csv (present in BEELINE but optional for this script)

------------| refNetwork.csv (Ground Truth of network1)

---------| dataset2 (another dataset link to network1 too)

------------| ExpressionData.csv (table of the expression of dataset2)

------------| PseudoTime.csv (present in BEELINE but optional for this script)

------------| refNetwork.csv (Ground Truth of network1)

---------| dataset. (etc)

------------| ...

------| network2 (another network to test)

---------| dataset1 (a dataset link to network2)

------------| ...

---------| dataset2 (another dataset link to network2 too)

------------| ...

---------| dataset. (etc)

------------| ...

------| network3 (etc)

---------| ...

4. Launch the script :

`python3 grn_run_and_eval.py base`

3 other options exist : NN, for comparing different NN architecture between them, Norm for adding normalisation comparison for base run/eval and Perm for evaluating permutations along 3 different metrics.

5. At the end of execution, find the evaluation scores as Heatmap or csv in the new "out" folder