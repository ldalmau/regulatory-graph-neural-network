import sys
#sys.path.append('/home/lisa/Workspace/code/')

import numpy as np
import pandas as pd
import anndata as ad
import os
import math
import random
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go

import scipy

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score, mean_absolute_error, r2_score, mean_absolute_percentage_error


from arboreto.algo import _prepare_input
from arboreto.utils import load_tf_names
from arboreto.core import to_tf_matrix,  clean

## Input functions

def read_exp_data(datatype, path):
    if datatype == "DREAM5":
        inf_reader = read_DREAM5
    elif datatype == "PIRIFORM":
        inf_reader = read_piriform
    elif datatype == "BEELINE":
        inf_reader = read_BEELINE
    elif datatype == "LUNG_CANCER":
        inf_reader = read_lung_cancer
    elif datatype == "HN_CANCER":
        inf_reader = read_hn_cancer

    expression_matrix = inf_reader(path)
    return expression_matrix


def read_BEELINE(path):
    # In Beeline, the genes are in columns
    expression_matrix = pd.read_csv(path, sep=",", index_col=0).T
    return expression_matrix

def rank_target_genes(rand_index, expression_matrix):
    mat = np.array(expression_matrix)
    mean_exp_list = []
    var_exp_list = []
    for i in rand_index : 
        exp = mat[:,i]
        mean_exp = np.mean(exp)
        mean_exp_list.append(mean_exp)
        var_exp = np.var(exp)
        var_exp_list.append(var_exp)
    mean_exp_list, rand_index, var_exp_list= zip(*sorted(zip(mean_exp_list, rand_index, var_exp_list)))
    return rand_index, mean_exp_list, var_exp_list

    

def prepare_data(DATA_TYPE, DATA_PATH, TF_PATH, N_iter_regressors):
    expression_matrix = read_exp_data(DATA_TYPE, DATA_PATH)
    gene_names = expression_matrix.columns
    if os.path.isfile(TF_PATH):
        print('TFs list is provided')
        tf_names = load_tf_names(TF_PATH)
        for i in range(len(tf_names)):
            #print(tf_names[i])
            tf_names[i] = tf_names[i].split(',')[0]
            #print(tf_names[i])
        #print(tf_names)
        #print(gene_names)
    else :
        tf_names = list(gene_names)
    
    tfs_in_matrix = set(tf_names).intersection(set(gene_names))
    print('{} transcription factors in common with expression matrix\n'.format(str(len(tfs_in_matrix))))
    expression_matrix, gene_names, tf_names = _prepare_input(expression_matrix, gene_names, tf_names)
    target_genes = 'all'
    assert expression_matrix.shape[1] == len(gene_names)
    tf_matrix, tf_matrix_gene_names = to_tf_matrix(expression_matrix, gene_names, tf_names)
    rand_index = random.sample(range(len(gene_names)), N_iter_regressors)
    rand_index, ordering_criterion, var_exp_list = rank_target_genes(rand_index,  expression_matrix)
    return gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, ordering_criterion, var_exp_list
    
    
def prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED):
    target_gene_name = gene_names[target_gene_index]
    target_gene_expression = expression_matrix[:, target_gene_index]
    (clean_tf_matrix, clean_tf_matrix_gene_names) = clean(tf_matrix, tf_matrix_gene_names, target_gene_name)
    x_train,x_test,y_train,y_test=train_test_split(clean_tf_matrix,target_gene_expression,test_size=0.25, 
                                                       random_state=RAND_SEED)
    
    return target_gene_name, x_train, x_test, y_train, y_test
    
    
    
## Metrics functions

def MAE(result_model):
    all_values = []
    for target_gene_name, max_feature, predictions, true in result_model:        
        all_values.append(mean_absolute_error(true, predictions))
    return all_values

def NMAE(result_model):
    all_values = []
    for target_gene_name, max_feature, predictions, true in result_model:
        #print(np.percentile(true, 75) - np.percentile(true, 25))
        #print(true)
        print(max(true))
        print(min(true))
        print(np.percentile(true, 75))
        print(np.percentile(true, 25))
        all_values.append(mean_absolute_error(true, predictions))
    return all_values

def RMSE(result_model):
    all_values = []
    for target_gene_name, max_feature, predictions, true in result_model:        
        all_values.append(math.sqrt(mean_squared_error(true, predictions)))
    return all_values


def R_square(result_model):
    all_values = []
    for target_gene_name, max_feature, prediction, true in result_model:
        all_values.append(r2_score(true, prediction))
    return all_values #r2_score(true_list, pred_list, multioutput='raw_values') -> the matrix should be built differently, it gives back the score for every cells instead of every gene


def MAPE(result_model):
    all_values = []
    for target_gene_name, max_feature, predictions, true in result_model:        
        all_values.append(mean_absolute_percentage_error(true, predictions))
    return all_values

def Exp_VAR(result_model):
    all_values = []
    for target_gene_name, max_feature, predictions, true in result_model:        
        all_values.append(explained_variance_score(true, predictions))
    return all_values
    
    
## Printing functions

import plotly.io as pio

pio.templates["draft"] = go.layout.Template(
    layout_plot_bgcolor = "#edf6f9",
    layout_paper_bgcolor = "#edf6f9",
    layout_font = dict(
        family="Latin Modern",
        size=15,
        color="#7a666f"
    ),
    
    layout_colorscale = dict(
    diverging=  [[0, '#8e0152'], [0.1, '#c51b7d'], [0.2,
                                 '#de77ae'], [0.3, '#f1b6da'], [0.4, '#fde0ef'],
                                 [0.5, '#f7f7f7'], [0.6, '#e6f5d0'], [0.7,
                                 '#b8e186'], [0.8, '#7fbc41'], [0.9, '#4d9221'],
                                 [1, '#276419']],
    sequential= [[0.0, '#035e66'], 
                [0.1111111111111111, '#00838f'], 
                [0.2222222222222222, '#42a4a7'],
                [0.3333333333333333, '#83c5be'],
                [0.4444444444444444, '#c1c4b6'],
                [0.5555555555555556, '#ffc2ad'],
                [0.6666666666666666, '#f5b4a3'],
                [0.7777777777777778, '#d88a83'],
                [0.8888888888888888, '#c46e6e'], 
                [1.0, '#9f6a6f']]
    ), 
    layout_hovermode = 'closest', 
)
pio.templates.default = str("draft")

pio.templates.default = str("draft")

def parcoords_for_one_model(df,  dico_ranges, title_fig, name):
    fig = px.parallel_coordinates(df, color="mean_expression", title = title_fig,
                  labels={ "mean_expression": "MEAN(exp)", 'var_exp': 'VAR(exp)',  
                          "r_square": "R²", "mae": "MAE","N_mae":"NMAEbyMean",
                          "rmse": "RMSE","N_rmse": "NRMSEbyMean", "explained_var": "Explained variance",
                          'max_feature_score': 'MAX(feat.sc.)fs', 'opposite_r_square': '-R²',"r_square_fs": "R² fs" , 
                          'opposite_r_square_fs': '-R² fs'},
                  #"adj_r": "adjusted R square" },
                  #color_continuous_scale=px.colors.diverging.Tealrose
                                 )
    
    for trace in range(len(fig["data"])):
            T = fig["data"][trace]
            for i in range(len(T['dimensions'])):
                dim = T['dimensions'][i]
                if  dim['label'] in dico_ranges.keys():
                    dim['range'] = dico_ranges[dim['label']]

            new_dim = go.Parcoords(dimensions = list([dict(tickvals = list(range(len(df['names']))), 
                                                      ticktext =  list(df['names']),
                                                       label = 'Target gene', 
                                                      values = list(range(len(df['names']))))]))
            
            REPLACEMENT = new_dim['dimensions'] + T['dimensions']
            T['dimensions'] =REPLACEMENT
    #fig.write_html("parallel_plot_{}.html".format('_'.join(title_fig.split(' '))))
    fig.show()
    #fig.write_image("images/{}.png".format(name), width=900, height=300)
    
    
def parallel_plot(list_model_result, rand_index, ordering_criterion, var_exp, data_type, data_name):
    list_df = []
    max_N_mae, max_N_rmse, max_mae, max_rmse, max_explained_var, max_r_square, max_feature_score, max_mean_exp, max_var_exp = -math.inf, -math.inf, -math.inf, -math.inf, -math.inf, -math.inf, -math.inf, -math.inf, -math.inf
    min_N_mae, min_N_rmse, min_mae, min_rmse, min_explained_var, min_r_square, min_feature_score, min_mean_exp, min_var_exp = math.inf,  math.inf,  math.inf,  math.inf,  math.inf, math.inf,  math.inf, math.inf,  math.inf

    for i in range(len(list_model_result)):
        res = list_model_result[i]
        list_mean = []
        for target_gene_name, max_feature, prediction, true in res:
            list_mean.append(np.mean(true))
        name_list = []
        max_feature_list = []
        for target in res:
            name_list.append(target[0])
            max_feature_list.append(target[1])
        list_mean = np.array(list_mean)
        n = len(max_feature_list)
        
        mae  = MAE(res)
        rmse = RMSE(res)
        N_mae = mae/list_mean
        N_rmse = rmse/list_mean
        explained_var = Exp_VAR(res)
        r_square = R_square(res)
        opposite_r_square = -1 * np.array(r_square)
        mean_exp = ordering_criterion
        
        max_mae = max(max(mae), max_mae)
        min_mae = min(min(mae), min_mae)
        max_rmse = max(max(rmse), max_rmse)
        min_rmse = min(min(rmse), min_rmse)
        max_N_mae = max(max(N_mae), max_N_mae)
        min_N_mae = min(min(N_mae), min_N_mae)
        max_N_rmse = max(max(N_rmse), max_N_rmse)
        min_N_rmse = min(min(N_rmse), min_N_rmse)
        max_explained_var = max(max(explained_var), max_explained_var)
        min_explained_var = min(min(explained_var), min_explained_var)
        max_r_square = max(max(r_square), max_r_square)
        min_r_square = min(min(r_square), min_r_square)
        max_feature_score = max(max(max_feature_list), max_feature_score)
        min_feature_score = min(min(max_feature_list), max_feature_score)
        max_mean_exp = max(max(mean_exp), max_mean_exp)
        min_mean_exp = min(min(mean_exp), min_mean_exp)
        
        
        max_var_exp = max(max(var_exp), max_var_exp)
        min_var_exp = min(min(var_exp), min_var_exp)
        
        df = pd.DataFrame(data={'names': name_list,'mean_expression': mean_exp, 'var_exp' : var_exp, 
                                'opposite_r_square': opposite_r_square, 'mae': mae, 
                                'rmse': rmse, 
                                })
                                #'adj_r': adj_r})
        list_df.append(df)

     
    dico_ranges = { 'MEAN(exp)':[min_mean_exp, max_mean_exp], 
                   'VAR(exp)'  :[min_var_exp, max_var_exp],
                   "MAE"       :[min_mae, max_mae],
                   "NMAEbyMean":[min_N_mae, max_N_mae], 
                   "RMSE"      :[min_rmse, max_rmse], 
                   "NRMSEbyMean":[min_N_rmse, max_N_rmse], 
                   "Explained variance":[min_explained_var, max_explained_var], 
                   "R²"        :[min_r_square, max_r_square], 
                   '-R²'       :[-max_r_square, -min_r_square]}
    
    reg = ['Random Forests', 'Gradient Tree Boosting', 'Extremely Randomized Trees', "Neural Network"]
    title = ['{} {} trained on random target genes'.format( n, reg[i]) for i in range(len(reg))]


    for ID in range(len(list_df)):
        DF = list_df[ID]
        parcoords_for_one_model(DF,  dico_ranges, title[ID], "{}_{}".format(data_name, reg[ID]))
