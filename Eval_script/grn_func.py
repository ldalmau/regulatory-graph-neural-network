# Import
# 	Run
import random
import copy
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor, ExtraTreesRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score, mean_absolute_error, r2_score, mean_absolute_percentage_error
from sklearn.inspection import permutation_importance
import tools_lisa 
from tools_lisa import prepare_data, prepare_for_regressor, parallel_plot
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from eli5.permutation_importance import get_score_importances
import numpy as np
import shap
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg') # Disable display, necessary due to WSL2 (Windows Subsystem for Linux) that can't open Linux windows

tf.get_logger().setLevel('ERROR') # Suppress a warning that is often display but not relevant

# 	Eval
import seaborn as sns
from pathlib import Path
sns.set(rc={"lines.linewidth": 2}, palette  = "deep", style = "ticks")
from sklearn.metrics import precision_recall_curve, roc_curve, auc
from itertools import product, permutations, combinations, combinations_with_replacement
from tqdm import tqdm
import argparse
import networkx as nx
import multiprocessing
from collections import defaultdict
from multiprocessing import Pool, cpu_count
from networkx.convert_matrix import from_pandas_adjacency
import yaml
import itertools

import os
import shutil


# 	tensorflow neural network architecture
def build_and_compile_nn_model(RAND_SEED):
	"""
	Model architecture from : DeepImpute: an accurate, fast, and scalable deep neural network method to impute single-cell RNA-seq data
	Cédric Arisdakessian, Olivier Poirion, Breck Yunits, Xun Zhu and Lana X. Garmire 
	"""
	model = keras.Sequential([
		layers.Dense(256, activation='relu'),
		layers.Dropout(0.2),
		layers.Dense(1)
	])
	
	tf.keras.utils.set_random_seed(RAND_SEED)

	model.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001))
	return model

# Def of functions :
# 	Import datasets
def find__iter(path) :
	"""
	Functions to find N_iter_regressors. If > 50, then set it to 50.
	"""
	data = pd.read_csv(path, sep=",", index_col=0)
	n_iter = data.shape[0]
	if n_iter > 50 :
		return 50
	else :
		return n_iter

#	Generate models and outputs
def compute_RF(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, RF_KWARGS):

	r2_list = []

	RF_path = os.path.join(path, "RF", network, dataset)
	if not Path(RF_path).exists() :
		os.makedirs(RF_path)
	RF_FI_path = os.path.join(path, "FI_RF", network, dataset)
	if not Path(RF_FI_path).exists() :
		os.makedirs(RF_FI_path)
	RF_path = os.path.join(path, "RF", network, dataset, "rankedEdges.csv")
	RF_FI_path = os.path.join(path, "FI_RF", network, dataset, "rankedEdges.csv")


	outFile = open(RF_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	outFile2 = open(RF_FI_path,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	for target_gene_index in tqdm(rand_index):
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		############### N_jobs can be set to a convenient value (-1 is suppose to automatically select the right number of cores to use)
		RF = RandomForestRegressor(random_state=RAND_SEED, n_jobs = RF_KWARGS["n_jobs"] , n_estimators = RF_KWARGS["n_estimators"],
                                   max_features = RF_KWARGS["max_features"]) # args are from arboreto/core.py
		RF.fit(x_train, y_train)

		predictions = RF.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))

    
		importances = permutation_importance(RF, x_test, y_test, random_state = RAND_SEED, scoring = "r2")
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		forest_importances = pd.Series(importances.importances_mean, index=feature_names).sort_values(0, ascending = False)
		# SHAP
		explainer = shap.Explainer(RF)
		shap_val = np.array(explainer.shap_values(x_test))
		
		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(forest_importances[j])])+'\n')
				j += 1

	outFile.close()
	outFile2.close()
	return r2_list

def compute_GBM(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, GBM_KWARGS):

	r2_list =[]

	GBM_path = os.path.join(path, "GBM", network, dataset)
	if not Path(GBM_path).exists() :
		os.makedirs(GBM_path)
	GBM_FI_path = os.path.join(path, "FI_GBM", network, dataset)
	if not Path(GBM_FI_path).exists() :
		os.makedirs(GBM_FI_path)
	GBM_path = os.path.join(path, "GBM", network, dataset, "rankedEdges.csv")
	GBM_FI_path = os.path.join(path, "FI_GBM", network, dataset, "rankedEdges.csv")


	outFile = open(GBM_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	outFile2 = open(GBM_FI_path,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		GBM = GradientBoostingRegressor(random_state=RAND_SEED, learning_rate= GBM_KWARGS["learning_rate"], 
										n_estimators = GBM_KWARGS["n_estimators"], max_features = GBM_KWARGS["max_features"]) # args are from arboreto/core.py
		#GBM.fit(x_train, y_train, monitor=EarlyStopMonitor(25)) # the lenght of the early stoping window is the same as in arboreto
		GBM.fit(x_train, y_train)
		predictions = GBM.predict(x_test)

		predictions = GBM.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))
		
		importances = permutation_importance(GBM, x_test, y_test, random_state = RAND_SEED, scoring = "r2")
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		forest_importances = pd.Series(importances.importances_mean, index=feature_names).sort_values(0, ascending = False)
		# SHAP
		explainer = shap.Explainer(GBM)
		shap_val = np.array(explainer.shap_values(x_test))

		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(forest_importances[j])])+'\n')
				j += 1

	outFile.close()
	outFile2.close()
	return r2_list

def compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, shap_expl = "deep") :

	r2_list = []

	NN_path = os.path.join(path, "NN", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN", network, dataset, "rankedEdges.csv")
	NN_path2 = os.path.join(path, "FI_NN", network, dataset)
	if not Path(NN_path2).exists() :
		os.makedirs(NN_path2)
	NN_path2 = os.path.join(path, "FI_NN", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	outFile2 = open(NN_path2,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	if shap_expl == "both" :
		NN_path3 = os.path.join(path, "NN_Ker", network, dataset)
		if not Path(NN_path3).exists() :
			os.makedirs(NN_path3)
		NN_path3 = os.path.join(path, "NN_Ker", network, dataset, "rankedEdges.csv")
		outFile3 = open(NN_path3,'w')
		outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		#input_layer = tf.keras.layers.InputLayer(input_shape=(x_train.shape[1],))
		if shap_expl == "both" or shap_expl == "kern" :
			X_train_summary = shap.kmeans(x_train, 10)
		
		dnn_scRNA_model = build_and_compile_nn_model(RAND_SEED)
		
		history = dnn_scRNA_model.fit(
			x_train,
			y_train,
			epochs=100,
			# Suppress logging.
			verbose=0,
			batch_size = 32,
			# Early stopping
			callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20),
			# Calculate validation results on 10% of the training data.
			validation_split = 0.1)
		
		predictions = dnn_scRNA_model.predict(x_test, verbose=0).flatten()
		r2_list.append(r2_score(y_test, predictions))

		# FI
		# Score for FI in NN
		def score(X, y):
			y_pred = dnn_scRNA_model.predict(X, verbose = 0).flatten()
			return r2_score(y, y_pred)
		base_score, score_decreases = get_score_importances(score, x_test, y_test)
		feature_importances = np.mean(score_decreases, axis=0)

		def f(X) :
			return dnn_scRNA_model.predict(X, verbose = 0).flatten()
		
		# SHAP
		if shap_expl == "deep" or shap_expl == "both" :
			explainer_deep = shap.DeepExplainer(dnn_scRNA_model, x_train)
			shap_val_deep = np.array(explainer_deep.shap_values(x_test)[0])

			shap_vector_deep = []
			for i in range(len(shap_val_deep[0])) :
				if np.count_nonzero(shap_val_deep[:,i]) == 0 :
					shap_vector_deep.append(0)
				else :
					shap_vector_deep.append(np.sum(abs(shap_val_deep[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
			shap_vector_deep = np.array(shap_vector_deep).T
		
		if shap_expl == "kern" or shap_expl == "both" :

			explainer_kern = shap.KernelExplainer(f, X_train_summary)
			shap_val_kern = np.array(explainer_kern.shap_values(x_test))
		
			shap_vector_kern = []
			for i in range(len(shap_val_kern[0])) :
				if np.count_nonzero(shap_val_kern[:,i]) == 0 :
					shap_vector_kern.append(0)
				else :
					shap_vector_kern.append(np.sum(abs(shap_val_kern[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
			shap_vector_deep = np.array(shap_vector_kern).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				if shap_expl == "kern" :
					outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_kern[j])])+'\n')
				else :
					outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_deep[j])])+'\n')

				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(feature_importances[j])])+'\n')

				if shap_expl == "both" :
					outFile3.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector_kern[j])])+'\n')
				j += 1

	outFile.close()
	outFile2.close()
	if shap_expl == "both" :
		outFile3.close()
	return r2_list

def compute_RF_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, RF_KWARGS):

	r2_list = []

	RF_path = os.path.join(path, "RF_norm", network, dataset)
	if not Path(RF_path).exists() :
		os.makedirs(RF_path)
	RF_path = os.path.join(path, "RF_norm", network, dataset, "rankedEdges.csv")


	outFile = open(RF_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	for target_gene_index in tqdm(rand_index):
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		############### N_jobs can be set to a convenient value (-1 is suppose to automatically select the right number of cores to use)
		RF = RandomForestRegressor(random_state=RAND_SEED, n_jobs = RF_KWARGS["n_jobs"] , n_estimators = RF_KWARGS["n_estimators"],
                                   max_features = RF_KWARGS["max_features"]) # args are from arboreto/core.py
		RF.fit(x_train, y_train)

		predictions = RF.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))

    
		importances = RF.feature_importances_
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		forest_importances = pd.Series(importances, index=feature_names).sort_values(0, ascending = False)
		# SHAP
		explainer = shap.Explainer(RF)
		shap_val = np.array(explainer.shap_values(x_test))
		
		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_GBM_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED, GBM_KWARGS):

	r2_list =[]

	GBM_path = os.path.join(path, "GBM_norm", network, dataset)
	if not Path(GBM_path).exists() :
		os.makedirs(GBM_path)
	GBM_path = os.path.join(path, "GBM_norm", network, dataset, "rankedEdges.csv")


	outFile = open(GBM_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		GBM = GradientBoostingRegressor(random_state=RAND_SEED, learning_rate= GBM_KWARGS["learning_rate"], 
										n_estimators = GBM_KWARGS["n_estimators"], max_features = GBM_KWARGS["max_features"]) # args are from arboreto/core.py
		#GBM.fit(x_train, y_train, monitor=EarlyStopMonitor(25)) # the lenght of the early stoping window is the same as in arboreto
		GBM.fit(x_train, y_train)
		predictions = GBM.predict(x_test)

		predictions = GBM.predict(x_test)
		r2_list.append(r2_score(y_test, predictions))
		
		importances = GBM.feature_importances_
		feature_names = copy.deepcopy(gene_names)
		feature_names.remove(target_gene_name)
		forest_importances = pd.Series(importances, index=feature_names).sort_values(0, ascending = False)
		# SHAP
		explainer = shap.Explainer(GBM)
		shap_val = np.array(explainer.shap_values(x_test))

		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_NN_wonorm(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED) :

	r2_list = []

	NN_path = os.path.join(path, "NN_norm", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN_norm", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		#input_layer = tf.keras.layers.InputLayer(input_shape=(x_train.shape[1],))
		
		dnn_scRNA_model = build_and_compile_nn_model(RAND_SEED)
		
		history = dnn_scRNA_model.fit(
			x_train,
			y_train,
			epochs=100,
			# Suppress logging.
			verbose=0,
			batch_size = 32,
			# Early stopping
			callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20),
			# Calculate validation results on 10% of the training data.
			validation_split = 0.1)
		
		predictions = dnn_scRNA_model.predict(x_test, verbose=0).flatten()
		r2_list.append(r2_score(y_test, predictions))
		
		# SHAP
		explainer = shap.DeepExplainer(dnn_scRNA_model, x_train)
		
		shap_val = np.array(explainer.shap_values(x_test)[0])
		
		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED) :

	r2_list = []

	NN_path = os.path.join(path, "NN_fun", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN_fun", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	for target_gene_index in tqdm(rand_index): 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)
		
		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		# Construction du modèle
		model1_input = keras.Input(shape=(x_train.shape[1],), name="M1")
		model2_input = keras.Input(shape=(x_train.shape[1],), name="M2")
		model3_input = keras.Input(shape=(x_train.shape[1],), name="M3")
		
		model1_pred = layers.Dense(256, activation='relu', name="M1_learn")(model1_input)
		model2_pred = layers.Dense(256, activation='relu', name="M2_learn")(model2_input)
		model3_pred = layers.Dense(256, activation='relu', name="M3_learn")(model3_input)
		
		model1_drop = layers.Dropout(0.2, name="M1_drop")(model1_pred)
		model2_drop = layers.Dropout(0.2, name="M2_drop")(model2_pred)
		model3_drop = layers.Dropout(0.2, name="M3_drop")(model3_pred)
		
		x = layers.concatenate([model1_drop, model2_drop, model3_drop])
		
		model1_out = layers.Dense(3, name="M1_out")(x)
		
		model = keras.Model(
		inputs=[model1_input, model2_input, model3_input],
		outputs=[model1_out],
		)
		
		model.compile(
		optimizer= tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001),
		loss='mean_squared_error',
		)
		
		tf.keras.utils.set_random_seed(RAND_SEED)
		
		# Découpage du dataset en 3
		m1_in, m2_in, m3_in = np.split(x_train, 3)
		
		m1_check, m2_check, m3_check = np.split(y_train, 3)
		new_y = np.zeros((m1_check.shape[0],3))
		for i in range(len(m1_check)) :
			new_y[i] = [m1_check[i], m2_check[i], m3_check[i]]
		
		
		model.fit(
		{"M1": m1_in, "M2": m2_in, "M3": m3_in},
		new_y,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		
		
		if x_test.shape[0] % 3 == 0 :
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		elif x_test.shape[0] % 3 == 1 :
			x_test = x_test[0:-1]
			y_test = y_test[0:-1]
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		else :
			x_test = x_test[0:-2]
			y_test = y_test[0:-2]
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		
		
		pred = model.predict((m1_pred, m2_pred, m3_pred), verbose = 0)
		pred1 = []
		pred2 = []
		pred3 = []
		for i in range(pred.shape[0]) :
			pred1.append(pred[i][0])
			pred2.append(pred[i][1])
			pred3.append(pred[i][2])
		   
		predictions = [*pred1, *pred2, *pred3]
		r2_list.append(r2_score(y_test, predictions))
		
		#Shap
		explainer = shap.DeepExplainer(model, [m1_in, m2_in, m3_in])
		
		shap_val = explainer.shap_values([m1_in, m2_in, m3_in])
		
		# On décompose les trois sorties
		shap_m1 = shap_val[0]
		shap_m2 = shap_val[1]
		shap_m3 = shap_val[2]
		
		# On fait la moyenne
		shap_val_m1 = sum(shap_m1)/len(shap_m1)
		shap_val_m2 = sum(shap_m2)/len(shap_m2)
		shap_val_m3 = sum(shap_m3)/len(shap_m3)
		
		# On reconstruit le vecteur avec toutes les cellules
		shap_val = np.array([*shap_val_m1, *shap_val_m2, *shap_val_m3])
		
		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_fun_same_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED) :

	r2_list = []

	NN_path = os.path.join(path, "NN_fun_same", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN_fun_same", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	for target_gene_index in tqdm(rand_index): 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)
		
		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		# Construction du modèle
		model1_input = keras.Input(shape=(x_train.shape[1],), name="M1")
		model2_input = keras.Input(shape=(x_train.shape[1],), name="M2")
		model3_input = keras.Input(shape=(x_train.shape[1],), name="M3")
		
		model1_pred = layers.Dense(256, activation='relu', name="M1_learn")(model1_input)
		model2_pred = layers.Dense(256, activation='relu', name="M2_learn")(model2_input)
		model3_pred = layers.Dense(256, activation='relu', name="M3_learn")(model3_input)
		
		model1_drop = layers.Dropout(0.2, name="M1_drop")(model1_pred)
		model2_drop = layers.Dropout(0.2, name="M2_drop")(model2_pred)
		model3_drop = layers.Dropout(0.2, name="M3_drop")(model3_pred)
		
		x = layers.concatenate([model1_drop, model2_drop, model3_drop])
		
		model1_out = layers.Dense(3, name="M1_out")(x)
		
		model = keras.Model(
		inputs=[model1_input, model2_input, model3_input],
		outputs=[model1_out],
		)
		
		model.compile(
		optimizer= tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001),
		loss='mean_squared_error',
		)
		
		tf.keras.utils.set_random_seed(RAND_SEED)

		new_y_train = [y_train, y_train, y_train]
		
		model.fit(
		{"M1": x_train, "M2": x_train, "M3": x_train},
		new_y_train,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		
		
		
		pred = model.predict((x_test, x_test, x_test), verbose = 0)
		predictions = (np.sum(pred, axis = 1))/len(pred[0])

		r2_list.append(r2_score(y_test, predictions))
		
		#Shap
		explainer = shap.DeepExplainer(model, [x_test, x_test, x_test])
		
		shap_val = np.array(explainer.shap_values([x_test, x_test, x_test]))
		
		# On fait la moyenne
		shap_val = sum(shap_val)/len(shap_val) # Pour chaque sortie 
		shap_val = sum(shap_val)/len(shap_val) # Pour chaque modèle

		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_fun_all_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED) :

	r2_list = []

	NN_path = os.path.join(path, "NN_fun_all", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN_fun_all", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')

	for target_gene_index in tqdm(rand_index): 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)
		
		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		# Construction du modèle
		model1_input = keras.Input(shape=(x_train.shape[1],), name="M1")
		model2_input = keras.Input(shape=(x_train.shape[1],), name="M2")
		model3_input = keras.Input(shape=(x_train.shape[1],), name="M3")
		
		model1_pred = layers.Dense(256, activation='relu', name="M1_learn")(model1_input)
		model2_pred = layers.Dense(256, activation='relu', name="M2_learn")(model2_input)
		model3_pred = layers.Dense(256, activation='relu', name="M3_learn")(model3_input)
		
		model1_drop = layers.Dropout(0.2, name="M1_drop")(model1_pred)
		model2_drop = layers.Dropout(0.2, name="M2_drop")(model2_pred)
		model3_drop = layers.Dropout(0.2, name="M3_drop")(model3_pred)
		
		x = layers.concatenate([model1_drop, model2_drop, model3_drop])
		
		model1_out = layers.Dense(3, name="M1_out")(x)
		
		model = keras.Model(
		inputs=[model1_input, model2_input, model3_input],
		outputs=[model1_out],
		)
		
		model.compile(
		optimizer= tf.keras.optimizers.Adam(0.0001, weight_decay = 0.0000001),
		loss='mean_squared_error',
		)
		
		tf.keras.utils.set_random_seed(RAND_SEED)
		
		# Découpage du dataset en 3
		m1_in, m2_in, m3_in = np.split(x_train, 3)

		x_train2 = np.array([*m2_in,*m3_in,*m1_in])
		x_train3 = np.array([*m3_in,*m1_in,*m2_in])
		
		m1_check, m2_check, m3_check = np.split(y_train, 3)

		y_train2 = np.array([*m2_check, *m3_check, *m1_check])
		y_train3 = np.array([*m3_check,*m1_check,*m2_check])

		new_y_train = np.array([y_train, y_train2, y_train3]).T

		model.fit(
		{"M1": x_train, "M2": x_train2, "M3": x_train3},
		new_y_train,
		epochs=100,
		batch_size=32,
		verbose=0,
		callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=10),
		validation_split = 0.1
		)
		
		
		if x_test.shape[0] % 3 == 0 :
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		elif x_test.shape[0] % 3 == 1 :
			x_test = x_test[0:-1]
			y_test = y_test[0:-1]
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		else :
			x_test = x_test[0:-2]
			y_test = y_test[0:-2]
			m1_pred, m2_pred, m3_pred = np.split(x_test, 3)
		
		x_test2 = np.array([*m2_pred,*m3_pred,*m1_pred])
		x_test3 = np.array([*m3_pred,*m1_pred,*m2_pred])

		
		pred = model.predict((x_test, x_test2, x_test3), verbose = 0)
		pred1 = []
		pred2 = []
		pred3 = []
		for i in range(pred.shape[0]) :
			pred1.append(pred[i][0])
			pred2.append(pred[i][1])
			pred3.append(pred[i][2])

		m1_inter, m2_inter, m3_inter = np.split(np.array(pred2), 3)
		pred2 = np.array([*m3_inter, *m1_inter, *m2_inter])

		m1_inter, m2_inter, m3_inter = np.split(np.array(pred3), 3)
		pred3 = np.array([*m2_inter, *m3_inter, *m1_inter])

		pred_inter = np.array([pred1, pred2, pred3])

		predictions = np.sum(pred_inter, axis = 0)/len(pred_inter)
		r2_list.append(r2_score(y_test, predictions))
		
		#Shap
		explainer = shap.DeepExplainer(model, [x_test, x_test2, x_test3])
		
		shap_val = explainer.shap_values([x_test, x_test2, x_test3])
		
		# On décompose les trois sorties
		shap_m1 = shap_val[0]
		shap_m2 = shap_val[1]
		shap_m3 = shap_val[2]

		# On fait la moyenne pour obtenir la valeur
		shap_m1 = sum(shap_m1)/len(shap_m1)
		shap_m2 = sum(shap_m2)/len(shap_m2)
		shap_m3 = sum(shap_m3)/len(shap_m3)

		# on remet 2 et 3 dans l'ordre
		m1_inter, m2_inter, m3_inter = np.split(shap_m2, 3)
		shap_m2 = [*m3_inter, *m1_inter, *m2_inter]

		m1_inter, m2_inter, m3_inter = np.split(shap_m3, 3)
		shap_m3 = [*m2_inter, *m3_inter, *m1_inter]
		
		# On fait la moyenne
		shap_val = np.array([shap_m1, shap_m2, shap_m3])
		shap_val = sum(shap_val)/len(shap_val)

		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				j += 1

	outFile.close()
	return r2_list

def compute_NN_FI(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, path, network, dataset, RAND_SEED) :

	r2_list = []

	NN_path = os.path.join(path, "NN", network, dataset)
	if not Path(NN_path).exists() :
		os.makedirs(NN_path)
	NN_path = os.path.join(path, "NN", network, dataset, "rankedEdges.csv")
	NN_path2 = os.path.join(path, "FI_NN_R2", network, dataset)
	if not Path(NN_path2).exists() :
		os.makedirs(NN_path2)
	NN_path2 = os.path.join(path, "FI_NN_R2", network, dataset, "rankedEdges.csv")
	NN_path3 = os.path.join(path, "FI_NN_MSE", network, dataset)
	if not Path(NN_path3).exists() :
		os.makedirs(NN_path3)
	NN_path3 = os.path.join(path, "FI_NN_MSE", network, dataset, "rankedEdges.csv")
	NN_path4 = os.path.join(path, "FI_NN_MAE", network, dataset)
	if not Path(NN_path4).exists() :
		os.makedirs(NN_path4)
	NN_path4 = os.path.join(path, "FI_NN_MAE", network, dataset, "rankedEdges.csv")

	outFile = open(NN_path,'w')
	outFile.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	outFile2 = open(NN_path2,'w')
	outFile2.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	outFile3 = open(NN_path3,'w')
	outFile3.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	outFile4 = open(NN_path4,'w')
	outFile4.write('Gene1'+'\t'+'Gene2'+'\t'+'EdgeWeight'+'\n')
	
	for target_gene_index in tqdm(rand_index) : 
		target_gene_name, x_train, x_test, y_train, y_test = prepare_for_regressor(target_gene_index, gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, RAND_SEED)

		# Norm
		y_train = (y_train - min(y_train))/max(y_train)
		y_test = (y_test - min(y_test))/max(y_test)

		#input_layer = tf.keras.layers.InputLayer(input_shape=(x_train.shape[1],))
		
		dnn_scRNA_model = build_and_compile_nn_model(RAND_SEED)
		
		history = dnn_scRNA_model.fit(
			x_train,
			y_train,
			epochs=100,
			# Suppress logging.
			verbose=0,
			batch_size = 32,
			# Early stopping
			callbacks = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20),
			# Calculate validation results on 10% of the training data.
			validation_split = 0.1)
		
		predictions = dnn_scRNA_model.predict(x_test, verbose=0).flatten()
		r2_list.append(r2_score(y_test, predictions))

		# FI
		# Score for FI in NN
		def score_r2(X, y):
			y_pred = dnn_scRNA_model.predict(X, verbose = 0).flatten()
			return r2_score(y, y_pred)
		base_score_r2, score_decreases_r2 = get_score_importances(score_r2, x_test, y_test)
		feature_importances_r2 = np.mean(score_decreases_r2, axis=0)

		def score_mse(X, y):
			y_pred = dnn_scRNA_model.predict(X, verbose = 0).flatten()
			return mean_squared_error(y, y_pred)
		base_score_mse, score_decreases_mse = get_score_importances(score_mse, x_test, y_test)
		feature_importances_mse = np.mean(score_decreases_mse, axis=0)

		def score_mae(X, y):
			y_pred = dnn_scRNA_model.predict(X, verbose = 0).flatten()
			return mean_absolute_error(y, y_pred)
		base_score_mae, score_decreases_mae = get_score_importances(score_mae, x_test, y_test)
		feature_importances_mae = np.mean(score_decreases_mae, axis=0)
		
		# SHAP
		explainer = shap.DeepExplainer(dnn_scRNA_model, x_train)
		
		shap_val = np.array(explainer.shap_values(x_test)[0])
		
		shap_vector = []
		for i in range(len(shap_val[0])) :
			if np.count_nonzero(shap_val[:,i]) == 0 :
				shap_vector.append(0)
			else :
				shap_vector.append(np.sum(abs(shap_val[:,i]), axis = 0) / np.count_nonzero(x_test[:,i]))
		shap_vector = np.array(shap_vector).T
		
		j = 0
		for i in range(len(gene_names)) :
			if gene_names[i] == target_gene_name :
				continue
			else :
				outFile.write('\t'.join([gene_names[i],target_gene_name,str(shap_vector[j])])+'\n')
				outFile2.write('\t'.join([gene_names[i],target_gene_name,str(feature_importances_r2[j])])+'\n')
				outFile3.write('\t'.join([gene_names[i],target_gene_name,str(feature_importances_mse[j])])+'\n')
				outFile4.write('\t'.join([gene_names[i],target_gene_name,str(feature_importances_mae[j])])+'\n')
				j += 1

	outFile.close()
	outFile2.close()
	outFile3.close()
	outFile4.close()
	return r2_list

# 	Evaluation of outputs
# 		AUROC AUPRC
def PRROC(true_path, compute_path, directed = False, selfEdges = False):
	'''
	Computes areas under the precision-recall and ROC curves
	for a given dataset for each algorithm.
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param directed:   A flag to indicate whether to treat predictionsas directed edges (directed = True) or undirected edges (directed = False).
	:type directed: bool
	
	:param selfEdges:   A flag to indicate whether to includeself-edges (selfEdges = True) or exclude self-edges (selfEdges = False) from evaluation.
	:type selfEdges: bool
	
	:param plotFlag:   A flag to indicate whether or not to save PR and ROC plots.
	:type plotFlag: bool
	
	:returns:
			- AUPRC: A dictionary containing AUPRC values for each algorithm
			- AUROC: A dictionary containing AUROC values for each algorithm
	'''
	
	# Read file for trueEdges
	trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"),sep = ',', 
								header = 0, index_col = None)
	
	# Initialize data list
	precisionDict = []
	recallDict = []
	FPRDict = []
	TPRDict = []
	AUPRC = []
	AUROC = []
	
	if directed:
		# check if the output rankedEdges file exists
		if Path(compute_path +'/rankedEdges.csv').exists():
			# Initialize Precsion
			predDF = pd.read_csv(compute_path + '/rankedEdges.csv', sep = '\t', header =  0, index_col = None)
			precisionDict, recallDict, FPRDict, TPRDict, AUPRC, AUROC = computeScores(trueEdgesDF, predDF, directed = True, selfEdges = selfEdges)
		else:
			print(compute_path +'/rankedEdges.csv', ' does not exist. Skipping...')
			PRName = '/PRplot'
			ROCName = '/ROCplot'
	else:
		# check if the output rankedEdges file exists
		if Path(compute_path + '/rankedEdges.csv').exists():
			# Initialize Precsion
			predDF = pd.read_csv(compute_path + '/rankedEdges.csv', sep = '\t', header =  0, index_col = None)
			precisionDict, recallDict, FPRDict, TPRDict, AUPRC, AUROC = computeScores(trueEdgesDF, predDF, directed = False, selfEdges = selfEdges)
		else:
			print(compute_path + '/rankedEdges.csv', ' does not exist. Skipping...')
			PRName = '/uPRplot'
			ROCName = '/uROCplot'

	return AUPRC, AUROC

def computeScores(trueEdgesDF, predEdgeDF, directed = True, selfEdges = True):
	'''        
	Computes precision-recall and ROC curves
	using scikit-learn for a given set of predictions in the 
	form of a DataFrame.
	Retrieve from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param trueEdgesDF:   A pandas dataframe containing the true classes.The indices of this dataframe are all possible edgesin a graph formed using the genes in the given dataset. This dataframe only has one column to indicate the classlabel of an edge. If an edge is present in the reference network, it gets a class label of 1, else 0.
	:type trueEdgesDF: DataFrame
	
	:param predEdgeDF:   A pandas dataframe containing the edge ranks from the prediced network. The indices of this dataframe are all possible edges.This dataframe only has one column to indicate the edge weightsin the predicted network. Higher the weight, higher the edge confidence.
	:type predEdgeDF: DataFrame
	
	:param directed:   A flag to indicate whether to treat predictionsas directed edges (directed = True) or undirected edges (directed = False).
	:type directed: bool
	:param selfEdges:   A flag to indicate whether to includeself-edges (selfEdges = True) or exclude self-edges (selfEdges = False) from evaluation.
	:type selfEdges: bool
	
	:returns:
			- prec: A list of precision values (for PR plot)
			- recall: A list of precision values (for PR plot)
			- fpr: A list of false positive rates (for ROC plot)
			- tpr: A list of true positive rates (for ROC plot)
			- AUPRC: Area under the precision-recall curve
			- AUROC: Area under the ROC curve
	'''
	if directed:        
		# Initialize dictionaries with all 
		# possible edges
		if selfEdges:
			possibleEdges = list(product(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), repeat = 2))
		else:
			possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		
		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth
		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[(trueEdgesDF['Gene1'] == key.split('|')[0]) &
					(trueEdgesDF['Gene2'] == key.split('|')[1])])>0:
					TrueEdgeDict[key] = 1
		
		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[(predEdgeDF['Gene1'] == key.split('|')[0]) &
								(predEdgeDF['Gene2'] == key.split('|')[1])]
		if len(subDF)>0:
			PredEdgeDict[key] = np.abs(subDF.EdgeWeight.values[0])

	# if undirected
	else:
		# Initialize dictionaries with all 
		# possible edges
		if selfEdges:
			possibleEdges = list(combinations_with_replacement(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		else:
			possibleEdges = list(combinations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth

		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[((trueEdgesDF['Gene1'] == key.split('|')[0]) &
							(trueEdgesDF['Gene2'] == key.split('|')[1])) |
								((trueEdgesDF['Gene2'] == key.split('|')[0]) &
							(trueEdgesDF['Gene1'] == key.split('|')[1]))]) > 0:
				TrueEdgeDict[key] = 1  

		# Compute PredEdgeDict Dictionary
		# from predEdgeDF

		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[((predEdgeDF['Gene1'] == key.split('|')[0]) &
								(predEdgeDF['Gene2'] == key.split('|')[1])) |
								((predEdgeDF['Gene2'] == key.split('|')[0]) &
								(predEdgeDF['Gene1'] == key.split('|')[1]))]
			if len(subDF)>0:
				PredEdgeDict[key] = max(np.abs(subDF.EdgeWeight.values))

		
		
	# Combine into one dataframe
	# to pass it to sklearn
	outDF = pd.DataFrame([TrueEdgeDict,PredEdgeDict]).T
	outDF.columns = ['TrueEdges','PredEdges']
	
	fpr, tpr, thresholds = roc_curve(y_true=outDF['TrueEdges'], y_score=outDF['PredEdges'], pos_label=1)

	prec, recall, thresholds = precision_recall_curve(y_true=outDF['TrueEdges'], probas_pred=outDF['PredEdges'], pos_label=1)
	
	return prec, recall, fpr, tpr, auc(recall, prec), auc(fpr, tpr)

#		EarlyPrecision
def EarlyPrec(true_path, compute_path, TFEdges = False):
	'''
	Computes early precision for a given algorithm for each dataset.
	We define early precision as the fraction of true 
	positives in the top-k edges, where k is the number of
	edges in the ground truth network (excluding self loops).
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline

	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: BLEval
	  
	:param algorithmName: Name of the algorithm for which the early precision is computed.
	:type algorithmName: str
	

	:returns:
		A dataframe containing early precision values
		for a given algorithm for each dataset.
	'''
	rankDict = []
	trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',
					header = 0, index_col = None)
	trueEdgesDF = trueEdgesDF.loc[(trueEdgesDF['Gene1'] != trueEdgesDF['Gene2'])]
	trueEdgesDF.drop_duplicates(keep = 'first', inplace=True)
	trueEdgesDF.reset_index(drop=True, inplace=True)


	rank_path = compute_path + "/rankedEdges.csv"
	if not os.path.isdir(compute_path):
		rankDict = set([])
	try:
		predDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
	except:
		print("\nSkipping early precision computation for ", algo, "on path", compute_path)
		rankDict = set([])

	predDF = predDF.loc[(predDF['Gene1'] != predDF['Gene2'])]
	predDF.drop_duplicates(keep = 'first', inplace=True)
	predDF.reset_index(drop=True, inplace=True)
	
	if TFEdges:
		# Consider only edges going out of TFs
		
		# Get a list of all possible TF to gene interactions 
		uniqueNodes = np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']])
		possibleEdges_TF = set(product(set(trueEdgesDF.Gene1),set(uniqueNodes)))

		# Get a list of all possible interactions 
		possibleEdges_noSelf = set(permutations(uniqueNodes, r = 2))
		
		# Find intersection of above lists to ignore self edges
		# TODO: is there a better way of doing this?
		possibleEdges = possibleEdges_TF.intersection(possibleEdges_noSelf)
		
		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		trueEdges = trueEdgesDF['Gene1'] + "|" + trueEdgesDF['Gene2']
		trueEdges = trueEdges[trueEdges.isin(TrueEdgeDict)]
		print("\nEdges considered ", len(trueEdges))
		numEdges = len(trueEdges)
		 
		predDF['Edges'] = predDF['Gene1'] + "|" + predDF['Gene2']
		# limit the predicted edges to the genes that are in the ground truth
		predDF = predDF[predDF['Edges'].isin(TrueEdgeDict)]

	else:
		trueEdges = trueEdgesDF['Gene1'] + "|" + trueEdgesDF['Gene2']
		trueEdges = set(trueEdges.values)
		numEdges = len(trueEdges)
		
		# check if ranked edges list is empty
		# if so, it is just set to an empty set

	if not predDF.shape[0] == 0:

		# we want to ensure that we do not include
		# edges without any edge weight
		# so check if the non-zero minimum is
		# greater than the edge weight of the top-kth
		# node, else use the non-zero minimum value.
		predDF.EdgeWeight = predDF.EdgeWeight.round(6)
		predDF.EdgeWeight = predDF.EdgeWeight.abs()

		# Use num True edges or the number of
		# edges in the dataframe, which ever is lower
		maxk = min(predDF.shape[0], numEdges)
		edgeWeightTopk = predDF.iloc[maxk-1].EdgeWeight

		nonZeroMin = np.nanmin(predDF.EdgeWeight.replace(0, np.nan).values)
		bestVal = max(nonZeroMin, edgeWeightTopk)

		newDF = predDF.loc[(predDF['EdgeWeight'] >= bestVal)]
		rankDict = set(newDF['Gene1'] + "|" + newDF['Gene2'])
	else:
		print("\nSkipping early precision computation for on path ", rank_path,"due to lack of predictions.")
		rankDict = set([])
	Eprec = []
	Erec = []
	if len(rankDict) != 0:
		intersectionSet = rankDict.intersection(trueEdges)
		Eprec = len(intersectionSet)/len(rankDict)
		Erec = len(intersectionSet)/len(trueEdges)
	else:
		Eprec = 0
		Erec = 0

	return(Eprec)

#		Jaccard
def Jaccard(true_path, compute_path, dataset):
	"""
	A function to compute median pairwirse Jaccard similarity index
	of predicted top-k edges for a given set of datasets (obtained from
	the same reference network). Here k is the number of edges in the
	reference network (excluding self loops). 
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: :obj:`BLEval`
	
	
	:param algorithmName: Name of the algorithm for which the Spearman correlation is computed.
	:type algorithmName: str
	
	
	:returns:
		- median: Median of Jaccard correlation values
		- mad: Median Absolute Deviation of  the Spearman correlation values
	"""

	rankDict = {}
	sim_names = []
	for dat in dataset:
		trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',header = 0, index_col = None)

		possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))

		TrueEdgeDict = {'|'.join(p):0 for p in possibleEdges}
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		# Compute TrueEdgeDict Dictionary
		# 1 if edge is present in the ground-truth
		# 0 if edge is not present in the ground-truth
		numEdges = 0
		for key in TrueEdgeDict.keys():
			if len(trueEdgesDF.loc[(trueEdgesDF['Gene1'] == key.split('|')[0]) &
					(trueEdgesDF['Gene2'] == key.split('|')[1])])>0:
				TrueEdgeDict[key] = 1
				numEdges += 1

		outDir = os.path.join(compute_path, dat)
		rank_path = os.path.join(outDir, "rankedEdges.csv")
		if not os.path.isdir(outDir):
			continue
		try:
			predDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
		except:
			print("Skipping Jaccard computation for ", algorithmName, "on path", outDir)
			continue

		predDF = predDF.loc[(predDF['Gene1'] != predDF['Gene2'])]
		predDF.drop_duplicates(keep = 'first', inplace=True)
		predDF.reset_index(drop = True,  inplace= True)
		# check if ranked edges list is empty
		# if so, it is just set to an empty set

		if not predDF.shape[0] == 0:

			# we want to ensure that we do not include
			# edges without any edge weight
			# so check if the non-zero minimum is
			# greater than the edge weight of the top-kth
			# node, else use the non-zero minimum value.
			predDF.EdgeWeight = predDF.EdgeWeight.round(6)
			predDF.EdgeWeight = predDF.EdgeWeight.abs()

			# Use num True edges or the number of
			# edges in the dataframe, which ever is lower
			maxk = min(predDF.shape[0], numEdges)
			edgeWeightTopk = predDF.iloc[maxk-1].EdgeWeight

			nonZeroMin = np.nanmin(predDF.EdgeWeight.replace(0, np.nan).values)
			bestVal = max(nonZeroMin, edgeWeightTopk)

			newDF = predDF.loc[(predDF['EdgeWeight'] >= bestVal)]
			rankDict[dat] = set(newDF['Gene1'] + "|" + newDF['Gene2'])
		else:
			rankDict[dat] = set([])

	Jdf = computePairwiseJacc(rankDict)
	df = Jdf.where(np.triu(np.ones(Jdf.shape),  k = 1).astype(np.bool))
	df = df.stack().reset_index()
	df.columns = ['Row','Column','Value']
	return(df.Value.median(),df.Value.mad())


def computePairwiseJacc(inDict):
	"""
	A helper function to compute all pairwise Jaccard similarity indices
	of predicted top-k edges for a given set of datasets (obtained from
	the same reference network). Here k is the number of edges in the
	reference network (excluding self loops). 
	Retrevied from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	
	:param inDict:  A dictionary contaninig top-k predicted edges  for each dataset. Here, keys are the dataset name and the values are the set of top-k edges.
	:type inDict: dict
	:returns:
		A dataframe containing pairwise Jaccard similarity index values
	"""
	jaccDF = {key:{key1:{} for key1 in inDict.keys()} for key in inDict.keys()}
	for key_i in inDict.keys():
		for key_j in inDict.keys():
			num = len(inDict[key_i].intersection(inDict[key_j]))
			den = len(inDict[key_i].union(inDict[key_j]))
			if den != 0:
				jaccDF[key_i][key_j] = num/den
			else:
				jaccDF[key_i][key_j] = 0
	return pd.DataFrame(jaccDF)

#		Spearman
def Spearman(true_path, compute_path, dataset):
	"""
	A function to compute median pairwirse Spearman correlation
	of predicted ranked edges, i.e., the outputs of different datasets
	generated from the same reference network, for a given algorithm.
	Adaptated from BEELINE BLEVAL module from : https://github.com/murali-group/Beeline
	
	:param evalObject: An object of class :class:`BLEval.BLEval`.
	:type evalObject: BLEval
	
	:param algorithmName: Name of the algorithm for which the Spearman correlation is computed.
	:type algorithmName: str
	
	
	:returns:
		- median: Median of Spearman correlation values
		- mad: Median Absolute Deviation of  the Spearman correlation values
	"""

	rankDict = {}
	sim_names = []
	for dat in dataset:
		trueEdgesDF = pd.read_csv(os.path.join(true_path, "refNetwork.csv"), sep = ',',
						header = 0, index_col = None)
		possibleEdges = list(permutations(np.unique(trueEdgesDF.loc[:,['Gene1','Gene2']]), r = 2))
		PredEdgeDict = {'|'.join(p):0 for p in possibleEdges}

		outDir = os.path.join(compute_path, dat)
		rank_path = os.path.join(outDir, "rankedEdges.csv")
		if not os.path.isdir(outDir):
			continue
		try:
			predEdgeDF = pd.read_csv(rank_path, sep="\t", header=0, index_col=None)
		except:
			print("Skipping spearman computation for ", algorithmName, "on path", outDir)
			continue

		for key in PredEdgeDict.keys():
			subDF = predEdgeDF.loc[(predEdgeDF['Gene1'] == key.split('|')[0]) &
							(predEdgeDF['Gene2'] == key.split('|')[1])]
			if len(subDF)>0:
				PredEdgeDict[key] = np.abs(subDF.EdgeWeight.values[0])
		rankDict[dat] = PredEdgeDict
		sim_names.append(dataset)
	df2 = pd.DataFrame.from_dict(rankDict)
	spearmanDF = df2.corr(method='spearman')


	df = spearmanDF.where(np.triu(np.ones(spearmanDF.shape),  k = 1).astype(np.bool))

	df = df.stack().reset_index()
	df.columns = ['Row','Column','Value']

	return(df.Value.median(),df.Value.mad())
