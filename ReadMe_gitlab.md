*** Regulatory graph neural network

This gitlab repository contain all file used for the intership.

- The step1_train_regressor jupyter notebook is the first file wich I work on. Authored first by Lisa, it allow me to begin testing implementing Neural networks inference in the same way as the Arboreto inference.

- saving_shapley_value is a folder that contain the computation of the Shapley value of the dyn_BF network for the RF algorithm. As their computation were slow, to avoid compute them reapeatly they have been saved to allow testing inference comparison.

- BoolODE-0.1 is the importation of the repository of BoolODE : https://github.com/Murali-group/BoolODE. It is use for creating the synthetic and curated dataset for the script. These dataset can go into the data folder for the scripts.

- The result folder contain the first heatmap generated after evaluating the infering method. The low and high refer to wich type of computation were done : low for low N_iterators of GBM and RF and high for Arboreto value of the N_iterators.

- Eval_script is the first attemp to made the evaluation. It contain most of the comparison possible except that the functionnal model is only restricted to 3.

- Result_script is an improved script, focused on generating the evaluations figures made on the report (heatmap and scatterplot). Code is more easy to read and had better comments. However all functionnalities from Eval_script weren't keep. Missing Normalisation comparison (base in the script is with normalisation) or the permutation importance use of score comparison.

- public : code accces for INSA teacher.

- Internship_all.zip contain all files used and generated from the internship, if you want to find something hidden or recover figures used in the report it should be inside.