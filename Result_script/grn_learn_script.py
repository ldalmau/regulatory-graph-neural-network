# Import
# 	Run
import random
import tools_lisa 
from tools_lisa import prepare_data, prepare_for_regressor, parallel_plot
import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
matplotlib.use('Agg') # Disable display, necessary due to WSL2 (Windows Subsystem for Linux) that can't open Linux windows
tf.get_logger().setLevel('ERROR') # Suppress a warning that is often display but not relevant
import seaborn as sns
from pathlib import Path
sns.set(rc={"lines.linewidth": 2}, palette  = "deep", style = "ticks")
from tqdm import tqdm
import os
import shutil
import argparse
import grn_build
import grn_eval
import grn_utils
import copy

# Search for args for method
# base : train and evaluate for all different algorithms
# functional : compute for different functional neural neural (base from 4NN to 10NN)
input_parser = argparse.ArgumentParser()
input_parser.add_argument("method", help="Algorithm to train and test (could be : base or functional)")
args = input_parser.parse_args()

if args.method == "base" or args.method == "functional" :
	script_method = args.method
else :
	script_method = base

# Set seed
RAND_SEED = 744 #Usefull to reproduct the results, the value can be changed
random.seed(RAND_SEED)

# Set parameters
#	Name for lisa_tools use
DATA_TYPE="BEELINE"
#	TF_file, not usefull for beeeline synthetic dataset
TF_PATH="all"
# 	scikit-learn random forest regressor
RF_KWARGS = {
    'n_jobs': 1,
    'n_estimators': 1000,
    'max_features': 'sqrt'
}

# 	scikit-learn gradient boosting regressor
GBM_KWARGS = {
    'learning_rate': 0.01,
    'n_estimators': 500,
    'max_features': 0.1
}


# Initialise env :
# Check for data to exist, if not please refer to the ReadMe
# Create an algo folder that contain output of the infering method
# Create an output folder that will contain heatmap and scatterplot of the evaluations
root = os.getcwd()

if not Path(root + "/data").exists() :
	print("'data' folder missing, please create the folder and fill it with datasets")
root_data = os.path.join(root, "data")

if not Path(root + "/algo").exists() :
	os.mkdir(os.path.join(root, "algo"))
root_algo = os.path.join(root, "algo")

if not Path(root + "/output").exists() :
	os.mkdir(os.path.join(root, "output"))
root_out = os.path.join(root, "output")

# Main 
networks = os.listdir(root_data)
"""
# To allow a step by step computation, ask for network to train
# If you want to compute all you can comment from here to line 91 and add an s to network at line 92
for_user = ""
for i in range(len(networks)) :
	for_user += "%i. %s \t" %(i ,networks[i]) 
compute = int(input("Wich network to train (number)? %s " %(for_user)))

network = networks[compute]

network_bar = tqdm([network])
R2_col = ["NN", "NND", "3NND-Sep", "3NND-Dup", "3NND-Perm", "RF" , "GBM"] # Stock name of the different algorithm wich R² is computed, RF as SH_RF, PI_RF and FI_RF has the same learning model

for net in network_bar : # We go through all networks, normally 1 but more if you had commented up
	network_bar.set_description("Processing %s network" %(net))

	# Initialise the time log file
	with open("Time_log.txt", "a") as f :
		f.write("Computing time for %s network \n" %(net))

	# Compute path to list all dataset of the network
	net_path = os.path.join(root_data, net)
	datasets = os.listdir(net_path)

	dataset_bar = tqdm(datasets)

	# Initialize R² container for the network
	net_R2 = pd.DataFrame(np.zeros((0, 7)), columns = R2_col)

	for dat in dataset_bar : # And we go through all dataset of the network
		dataset_bar.set_description("Processing %s dataset" %(dat))

		# We get the file of the expression file and find his number of gene inside
		dat_path = os.path.join(net_path, dat)
		expr_path = os.path.join(dat_path, "ExpressionData.csv")
		N_iter_regressors = grn_utils.find__iter(expr_path)

		# Recover datas from the expression table
		gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, ordering_criterion, var_exp_list = prepare_data(DATA_TYPE, expr_path, TF_PATH, N_iter_regressors)
		
		if script_method == "base" :
			r2_NN_simp,  r2_NN_comp = grn_build.compute_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_3NN_sep, r2_3NN_dup, r2_3NN_perm = grn_build.compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED)
			r2_RF = grn_build.compute_RF(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, RF_KWARGS)
			r2_GBM = grn_build.compute_GBM(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, GBM_KWARGS)

			net_R2.loc[len(net_R2.index)] = [sum(r2_NN_simp)/len(r2_NN_simp), sum(r2_NN_comp)/len(r2_NN_comp), sum(r2_3NN_sep)/len(r2_3NN_sep), sum(r2_3NN_dup)/len(r2_3NN_dup), sum(r2_3NN_perm)/len(r2_3NN_perm), sum(r2_RF)/len(r2_RF), sum(r2_GBM)/len(r2_GBM)]
		elif script_method == "functional" :
			for num in tqdm(range(4,11)) :
				grn_build.compute_fun_NN(gene_names, expression_matrix, tf_matrix, tf_matrix_gene_names, rand_index, root_algo, net, dat, RAND_SEED, number = num)

	# When training is complete for all dataset export mean R²
	if not Path(os.path.join(root_algo, "R2_folder", net)).exists() :
		os.makedirs(os.path.join(root_algo, "R2_folder", net))
	#filename = os.path.join(root_algo, "R2_folder", net, "R2_score.csv")
	net_R2.to_csv(filename)

print("Training complete")
"""
# Compute AUROC, AUPRC and EalryPrecision + Jaccard and Spearman

algorithm = os.listdir(root_algo)
algorithm.remove("R2_folder") # Not an algorithm to test
algorithm.sort() # To keep always the same order between networks

#Prepare global plot
ref = "GRNBoost2" # Change this to made the scatterplot to another reference
# Separate SH and PI method for scatter plot
to_plot_SH = ["SH_GBM","SH_RF","SH_NN","SH_NND", "SH_3NND-Sep", "SH_3NND-Dup", "SH_3NND-Perm"]
to_plot_PI = ["PI_GBM","PI_RF","PI_NN","PI_NND", "PI_3NND-Sep", "PI_3NND-Dup", "PI_3NND-Perm"]
# To adapt in a better way : if name as NND-Dup ...
to_plot_multi_SH = ["SH_3NND-Sep", "SH_3NND-Dup", "SH_3NND-Perm", "SH_4NND-Sep", "SH_4NND-Dup", "SH_4NND-Perm", "SH_5NND-Sep", "SH_5NND-Dup", "SH_5NND-Perm", "SH_6NND-Sep", "SH_6NND-Dup", "SH_6NND-Perm", "SH_7NND-Sep", "SH_7NND-Dup", "SH_7NND-Perm"]
to_plot_multi_PI = ["PI_3NND-Sep", "PI_3NND-Dup", "PI_3NND-Perm", "PI_4NND-Sep", "PI_4NND-Dup", "PI_4NND-Perm", "PI_5NND-Sep", "PI_5NND-Dup", "PI_5NND-Perm", "PI_6NND-Sep", "PI_6NND-Dup", "PI_6NND-Perm", "PI_7NND-Sep", "PI_7NND-Dup", "PI_7NND-Perm"]
# Name of the metrics to build the pandas dataframe colomn names
to_heat = ["R²", "AUROC", "AUPRC", "EarlPrec","Jacc", "Spear"]
to_scatter = ["AUPRC", "EarlPrec", "Std_AUPRC", "Std_EarlPrec", "Network", "Algorithm"]

# Create the dataset for plotting more easily
data_plot_SH = pd.DataFrame(np.zeros((0, len(to_scatter))), columns = to_scatter)
data_plot_PI = pd.DataFrame(np.zeros((0, len(to_scatter))), columns = to_scatter)
data_plot_multi_SH = pd.DataFrame(np.zeros((0, 4)), columns = ["AUPRC", "EarlPrec", "Algorithm", "Name"])
data_plot_multi_PI = pd.DataFrame(np.zeros((0, 4)), columns = ["AUPRC", "EarlPrec", "Algorithm", "Name"])

network_bar = tqdm(networks)
for net in network_bar : # We evaluate for each network
	network_bar.set_description("Evaluating %s network" %(net))

	precision_dataset = pd.DataFrame(np.zeros((8, len(algorithm))), index = ["R²", "AUROC", "AUPRC", "EarlPrec", "Std_AUPRC", "Std_EarlPrec", "Jacc", "Spear"], columns = algorithm)
	can_compute_Jac = True

	
	# Fill R2 from R² file
	filename = os.path.join(root_algo, "R2_folder", net, "R2_score.csv")
	net_R2 = pd.read_csv(filename)
	net_R2 = net_R2.mean()
	for name in algorithm :
		if "4NN" not in name and "5NN" not in name and "6NN" not in name and "7NN" not in name and "8NN"  not in name and "9NN" not in name and "10NN" not in name : # not stored so doesn't want to search for them
			precision_dataset[name]["R²"] = net_R2[name[3:]]
	


	for algo in algorithm : # and for all algorithm
		search_dir = os.path.join(root_algo, algo, net)
		num_data = os.listdir(search_dir)
		if len(num_data) == 1 : # check if we have 1 or more dataset
			true_path = os.path.join(root_data, net, num_data[0])
			compute_path = os.path.join(root_algo, algo, net, num_data[0])
			AUPRC, AUROC = grn_eval.PRROC(true_path, compute_path, selfEdges = False)
			EarlyPrecision = grn_eval.EarlyPrec(true_path, compute_path, TFEdges = False)
			precision_dataset[algo]["AUROC"] = AUROC
			precision_dataset[algo]["AUPRC"] = AUPRC
			precision_dataset[algo]["EarlPrec"] = EarlyPrecision
			can_compute_Jac = False

		else :
			aur_list = []
			aup_list = []
			ear_list = []
			for i in range(len(num_data)) :
				true_path = os.path.join(root_data, net, num_data[i])
				compute_path = os.path.join(root_algo, algo, net, num_data[i])
				AUPRC, AUROC = grn_eval.PRROC(true_path, compute_path, directed = False, selfEdges = False)
				EarlyPrecision = grn_eval.EarlyPrec(true_path, compute_path, TFEdges = False)
				aur_list.append(AUROC)
				aup_list.append(AUPRC)
				ear_list.append(EarlyPrecision)
			
			precision_dataset[algo]["AUROC"] = sum(aur_list)/len(aur_list)
			precision_dataset[algo]["AUPRC"] = sum(aup_list)/len(aup_list)
			precision_dataset[algo]["EarlPrec"] = sum(ear_list)/len(ear_list)
			precision_dataset[algo]["Std_AUPRC"] = np.std(aup_list)
			precision_dataset[algo]["Std_EarlPrec"] = np.std(ear_list)
			# Compute Jaccard / Spearman
			true_path = os.path.join(root_data, net, num_data[0])
			compute_path = os.path.join(root_algo, algo, net)
			Jacc_med, Jacc_mad = grn_eval.Jaccard(true_path, compute_path, num_data)
			Spear_med, Spear_mad = grn_eval.Spearman(true_path, compute_path, num_data)
			precision_dataset[algo]["Jacc"] = Jacc_med
			precision_dataset[algo]["Spear"] = Spear_med



	if can_compute_Jac == False : # If absent remove from the dataframe, so will be removed from heatmap
		print("Cannot compute Jaccard and Spearman for ", net, " , need more dataset to evaluate")
		precision_dataset.drop(["Jacc", "Spear"], inplace=True)
		to_heat.remove("Jacc")
		to_heat.remove("Spear")


	# Rename for plotting Genie3 (FI_RF) and GRNBoost2 (FI_GBM)
	precision_dataset.rename(columns = {'FI_RF':'Genie3', 'FI_GBM':'GRNBoost2'}, inplace = True)

	# 1st : Write to file
	filename = "all_" + net + ".csv"
	precision_dataset.to_csv(os.path.join(root_out, filename))

	# 2nd : Heatmap

	######################### Arboreto vs NN ####################################

	data_out = precision_dataset.loc[to_heat, ["Genie3", "GRNBoost2"]] # Arboreto
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 16) # Set fontsize and rottion for better display
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "Arboreto_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))

	data_out = precision_dataset.loc[to_heat, ["SH_NN", "SH_NND"]] # NN
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 16)
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "NN_start_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))

	######################### TreeShap vs NNShap ####################################

	data_out = precision_dataset.loc[to_heat, ["SH_RF", "SH_GBM"]] # TreeShap
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 16)
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "Tree_shap_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))

	data_out = precision_dataset.loc[to_heat, ["SH_NN", "SH_NND", "SH_3NND-Sep", "SH_3NND-Dup", "SH_3NND-Perm"]] # NNShap
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 16)
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "NN_all_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))

	######################### TreeShap vs Treepermutation (annexe) ####################################

	# Already compute in TreeShap vs NNShap : Tree_shap_X.png

	data_out = precision_dataset.loc[to_heat, ["PI_RF", "PI_GBM"]] # TreePermutation
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 16)
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "Tree_PI_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))

	######################### NNShap vs NNpermutation ####################################

	# Already compute in TreeShap vs NNShap : NN_all_X.png

	data_out = precision_dataset.loc[to_heat, ["PI_NN", "PI_NND", "PI_3NND-Sep", "PI_3NND-Dup", "PI_3NND-Perm"]] # NNPermutation
	plt.figure(figsize=(9,8))
	ax_fig = sns.heatmap(data_out, annot=True, vmin = 0, vmax = 1, linewidths=.01, annot_kws={"fontsize":16})
	ax_fig.set_xticklabels(ax_fig.get_xticklabels(), rotation = 15, fontsize = 10)
	ax_fig.set_yticklabels(ax_fig.get_yticklabels(), rotation = 10, fontsize = 16)
	filename = "NN_PI_" + net + ".png"
	plt.savefig(os.path.join(root_out, filename))
	
	######################### Treepermutation vs NNpermutation ####################################

	# Already compute : Tree_PI_X.png, NN_PI_X.png 
	plt.close("all") # For memory save



	# Fill tha dataset for final plot containing difference of the algo from the reference
	if script_method == "base" :
		for name in to_plot_SH :
			precision_dataset[name]["AUPRC"] = precision_dataset[name]["AUPRC"] - precision_dataset[ref]["AUPRC"]
			precision_dataset[name]["EarlPrec"] = precision_dataset[name]["EarlPrec"] - precision_dataset[ref]["EarlPrec"]
			data_plot_SH.loc[len(data_plot_SH.index)] = [precision_dataset[name]["AUPRC"], precision_dataset[name]["EarlPrec"], precision_dataset[name]["Std_AUPRC"], precision_dataset[name]["Std_EarlPrec"], net, name]

		for name in to_plot_PI:
			precision_dataset[name]["AUPRC"] = precision_dataset[name]["AUPRC"] - precision_dataset[ref]["AUPRC"]
			precision_dataset[name]["EarlPrec"] = precision_dataset[name]["EarlPrec"] - precision_dataset[ref]["EarlPrec"]
			data_plot_PI.loc[len(data_plot_PI.index)] = [precision_dataset[name]["AUPRC"], precision_dataset[name]["EarlPrec"], precision_dataset[name]["Std_AUPRC"], precision_dataset[name]["Std_EarlPrec"], net, name]

	if script_method == "functional" :
		for name in to_plot_multi_SH:
			precision_dataset[name]["AUPRC"] = precision_dataset[name]["AUPRC"] - precision_dataset[ref]["AUPRC"]
			precision_dataset[name]["EarlPrec"] = precision_dataset[name]["EarlPrec"] - precision_dataset[ref]["EarlPrec"]
			data_plot_multi_SH.loc[len(data_plot_multi_SH.index)] = [precision_dataset[name]["AUPRC"], precision_dataset[name]["EarlPrec"], name[3:7], name[8:]]

		for name in to_plot_multi_PI:
			precision_dataset[name]["AUPRC"] = precision_dataset[name]["AUPRC"] - precision_dataset[ref]["AUPRC"]
			precision_dataset[name]["EarlPrec"] = precision_dataset[name]["EarlPrec"] - precision_dataset[ref]["EarlPrec"]
			data_plot_multi_PI.loc[len(data_plot_multi_PI.index)] = [precision_dataset[name]["AUPRC"], precision_dataset[name]["EarlPrec"], name[3:7], name[8:]]

if script_method == "base" :
	data_plot_SH.rename(columns = {'AUPRC':'AUPRC value of network - AUPRC value of Reference', 'EarlPrec':'EarlPrec value of network - EarlPrec value of Reference'}, inplace = True)
	plt.figure(figsize=(20,8))
	ax_fig = sns.scatterplot(data=data_plot_SH, x="AUPRC value of network - AUPRC value of Reference", y="EarlPrec value of network - EarlPrec value of Reference", 
		hue="Algorithm", style="Network", legend = "brief", s = 200, zorder = 2)
	ax_fig.set_xlim([-0.2, 0.8])
	ax_fig.set_ylim([-0.3, 0.6])
	plt.legend(title = "Reference : %s" %(ref), fontsize = 10, title_fontsize = 10) # Make legend
	sns.move_legend(ax_fig, "upper left", bbox_to_anchor=(1, 1)) # Move legend outside the box
	plt.errorbar("AUPRC value of network - AUPRC value of Reference", "EarlPrec value of network - EarlPrec value of Reference", xerr = "Std_AUPRC", yerr = "Std_EarlPrec", fmt='None', color = "black", data = data_plot_SH, zorder= 1)
	plt.axhline(color = "black", linestyle = "--") # Draw x = 0 line
	plt.axvline(color = "black", linestyle = "--") # Draw y = 0 line
	filename = "Final_SH.png"
	plt.savefig(os.path.join(root_out, filename))

	data_plot_PI.rename(columns = {'AUPRC':'AUPRC value of network - AUPRC value of Reference', 'EarlPrec':'EarlPrec value of network - EarlPrec value of Reference'}, inplace = True)
	plt.figure(figsize=(20,8))
	ax_fig = sns.scatterplot(data=data_plot_PI, x="AUPRC value of network - AUPRC value of Reference", y="EarlPrec value of network - EarlPrec value of Reference", 
		hue="Algorithm", style="Network", legend = "brief", s = 200, zorder = 2)
	ax_fig.set_xlim([-0.2, 0.8])
	ax_fig.set_ylim([-0.3, 0.6])
	plt.legend(title = "Reference : %s" %(ref), fontsize = 10, title_fontsize = 10)
	sns.move_legend(ax_fig, "upper left", bbox_to_anchor=(1, 1))
	plt.errorbar("AUPRC value of network - AUPRC value of Reference", "EarlPrec value of network - EarlPrec value of Reference", xerr = "Std_AUPRC", yerr = "Std_EarlPrec", fmt='None', color = "black", data = data_plot_PI, zorder= 1)
	plt.axhline(color = "black", linestyle = "--")
	plt.axvline(color = "black", linestyle = "--")
	filename = "Final_PI.png"
	plt.savefig(os.path.join(root_out, filename))

if script_method == "functional" :
	plt.figure(figsize=(20,8))
	ax_fig = sns.scatterplot(data=data_plot_multi_SH, x="AUPRC", y="EarlPrec", hue="Algorithm", style="Name", legend = "brief", s = 200, zorder = 2, )
	plt.legend(title = "Reference : %s \n AUPRC value : %.2f \n EarlyPrec value : %.2f" %(ref, precision_dataset[ref]["AUPRC"], precision_dataset[ref]["EarlPrec"]), fontsize = 10, title_fontsize = 10)
	sns.move_legend(ax_fig, "upper left", bbox_to_anchor=(1, 1))
	plt.axhline(color = "black", linestyle = "--")
	plt.axvline(color = "black", linestyle = "--")
	filename = "Final_multi_SH.png"
	plt.savefig(os.path.join(root_out, filename))

	plt.figure(figsize=(20,8))
	ax_fig = sns.scatterplot(data=data_plot_multi_PI, x="AUPRC", y="EarlPrec", hue="Algorithm", style="Name", legend = "brief", s = 200, zorder = 2)
	plt.legend(title = "Reference : %s \n AUPRC value : %.2f \n EarlyPrec value : %.2f" %(ref, precision_dataset[ref]["AUPRC"], precision_dataset[ref]["EarlPrec"]), fontsize = 10, title_fontsize = 10)
	sns.move_legend(ax_fig, "upper left", bbox_to_anchor=(1, 1))
	plt.axhline(color = "black", linestyle = "--")
	plt.axvline(color = "black", linestyle = "--")
	filename = "Final_multi_PI.png"
	plt.savefig(os.path.join(root_out, filename))