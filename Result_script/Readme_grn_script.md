** How to use grn_learn_script.py :

1. Setup a new conda environnement with the environnement.yml

`conda env create -f environment.yml`

and activate it

`conda activate tensorflow-env`

2. In the same folder as grn_learn_script.py, create a new folder "data" wich will contain the datasets to test

3. Add datasets in the "data" folder.
Beware, the dataset folder architecture should look like :

---| data (not part of the dataset)

------| network1 (a network to test)

---------| dataset1 (a dataset link to network1)

------------| ExpressionData.csv (table of the expression of dataset1)

------------| PseudoTime.csv (present in BEELINE but optional for this script)

------------| refNetwork.csv (Ground Truth of network1)

---------| dataset2 (another dataset link to network1 too)

------------| ExpressionData.csv (table of the expression of dataset2)

------------| PseudoTime.csv (present in BEELINE but optional for this script)

------------| refNetwork.csv (Ground Truth of network1)

---------| dataset. (etc)

------------| ...

------| network2 (another network to test)

---------| dataset1 (a dataset link to network2)

------------| ...

---------| dataset2 (another dataset link to network2 too)

------------| ...

---------| dataset. (etc)

------------| ...

------| network3 (etc)

---------| ...

4. Launch the script :

`python3 grn_learn_script.py`

At launch the program will request the number of the network to compute. As computation are slow, it allow to add and compute data 1 by 1.

5. At the end of execution, find the evaluation scores as Heatmap or csv and Final_figure in the new "output" folder

Beware if you use base and functional make sure to compute for all network present in the data folder or else you can retrieve them from the folder.
At the contrary, if you want to return to base but you have functionnal not for all data, remove the 4 to 10 NND-Sep/Perm/Dup folder from algo.