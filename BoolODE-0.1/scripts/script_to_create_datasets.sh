# Set the path to your clone of the BoolODE repository here
# The current options will perform stochastic simulations
# using the default kinetic parameters.
# If you would like to sample parameters, use the following options:
#  --sample-par --std 0.05 -i
# The above will specify a standard deviation of 5% of the default parameter
# value, and the -i option will set all parameters to a single sampled value.
# In order to generate samples, we use the scripts/genSamples.py
# The -d option specified number of datasets to generate (Set to 1)
# Each dataset will have a sample of 500 cells from the full set of 2000 simulations
# This is specified using the -n option.

numcells="600"
numcellssample="500"
numdatasets="1"

# Synthetic Networks
output_dir="Synthetic/"

# Linear
output_name="dyn_BF"
model_name="dyn_bifurcating"
echo "Simulating "$model_name
python src/BoolODE.py --path data/dyn-bifurcating.txt\
       --ics data/dyn-bifurcating_ics.txt\
       --strengths data/dyn-bifurcating_strengths.txt\
       --max-time 5\
       --num-cells $numcells\
       --do-parallel\
       --outPrefix $output_dir$output_name"/"\
       --sample-cells\
       --burn-in\
       #--add-dummy


echo ""
echo "Generating dropouts"

for quantile in 0.25 0.5
  do for proba in 0.7 0.9
    do
  python scripts/genDropouts.py  -e $output_dir"/"$output_name"/ExpressionData.csv"\
         -p $output_dir"/"$output_name"/PseudoTime.csv"\
         -r $output_dir"/"$output_name"/refNetwork.csv"\
         -n $numcellssample -d --drop-cutoff $quantile --drop-prob $proba -i 1\
         --outPrefix $output_dir"/"$output_name"/"$output_name
  done
done
